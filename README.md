---
title: Introduzione all'Intelligenza Artificiale
subtitle: Manabile del Corso di Informatica dell'Università di Pisa
author: Luca Canessa
date: "2021/2022"
output: 
    pdf_document:
        toc: true
        toc_depth: 6
        number_sections: true
        highlight: tango
language: "it"
fig_width: 6 
fig_height: 4
header-includes: 
    - \usepackage{xcolor}
    - \usepackage[ruled]{algorithm2e}
    - \usepackage{tcolorbox}
    - \usepackage[italian]{babel}
    - \usepackage{lmodern}
    - \renewcommand*\familydefault{\ttdefault}
    - \usepackage[T1]{fontenc}
documentclass: book
classoption: oneside
geometry: margin=1in

---

\newpage

# Risoluzione di Problemi come Ricerca

## Agenti

Gli agenti intelligenti sono **_situati_**, hanno **_abilità sociali_** e possono avere opinioni, obiettivi, emozioni ed essere embodied. \newline
Un agente razionale interagisce con il suo ambiente in maniera efficace e per valutarne la sua efficienza serve uno strumento di valutazione oggettivo come una misura esterna o scelta dal progettista o valutata su ambienti diversi.

> **Un agente razionale per ogni sequenza di percezioni compie l'azione che massimizza il valore atteso della misura delle prestazioni, considerando le sue percezioni passate e la sua conoscenza pregressa.**

### Descrizione dell'agente e dell'ambiente

#### Agente

Descrizione **PEAS**: 

- Performance(Prestazioni)
- Environment(Ambiente)
- Actuators(Attuatori)
- Sensors(Sensori)

 $$Agente: Percezioni \rightarrow Azioni$$ $$Agente = Architettura + Programma$$

*Tipi di agenti*:

1.  **basati su tabella**: azione scelta attraverso una tabella di azioni e percezioni
2.  **reattivi semplici**: azioni scelte in base alle percezioni attuali, tralasciano storia
3.  **basati su modello**: agente mantiene uno stato interno che dipende dalla storia delle percezioni
4.  **con obiettivo**: agente con modello e obiettivo che deve raggiungere
5.  **con funzione di utilità**: agente sceglie l'azione che massimizza la fun. di utilità, (fun di utilità è una misura di utilità presa con le condizioni attuali)
6.  **che apprendono**

*Tipi di stati in cui si trova l'agente*:

1.  Atomica
2.  fattorizzata
3.  strutturata

#### Ambiente

1.  Completamente o Parzialmente osservabile
2.  Singolo o Multi agente
3.  Deterministico o Stocastico o Non Deterministico
4.  Episodico o Sequenziale
5.  Statico o Dinamico
6.  Discreto o Continuo

---

---

## Problem Solving

Risoluzione di problemi viene vista come ricerca nello spazio degli stati. **Agenti con modello** che adottano la *rappresentazione atomica dello stato* e **agenti con obiettivo** che *pianificano* tutta la sequenza delle *mosse* prima di eseguirle.

### Modo di procedere

1. determinazione dell'obiettivo ($\implies$ stati in cui l'obiettivo è soddisfatto)
2. formulazione del problema ($\implies$ rappresentazione degli stati $\land$ rappresentazione delle azioni)
3. determinazione della soluzione mediante ricerca ($\implies$ piano di lavoro)
4. esecuzione del piano

Assumendo che l'ambiente sia *statico, osservabile, discreto, deterministico*, **la soluzione a qualsiasi problema è una sequenza fissata di azioni.**

#### Formulazione del problema

1. stato iniziale $s_{0}$
2. azioni possibili nello stato $s_{i}$
3. modello di transazione
	- risultato: $stato \times azione \rightarrow stato$
	- $risultato(s_{i}, a) = s_{i+1}$ 
4. test obiettivo $goal$
5. costo del cammino

(Punti *1,2,3* definisco lo spazio degli stati)

---

### Algoritmi di Ricerca

> Un processo che cerca una sequenza di azioni per raggiungere un obiettivo è detto ricerca

#### Misura delle prestazioni

$$CostoTotale = costoRicerca + costoCamminoSoluzione$$

Si valuterà la "bontà" dell'algoritmo basandosi sul valore del $costoRicerca$ ma si ottimizzerà il tutto andando a migliorare il valore del $costoCamminoSoluzione$

#### Alberi di ricerca

Albero di ricerca, generato da possibili sequenze di azioni, viene sovrapposto allo spazio degli stati. 

**NODO** $\neq$ **STATO**

(frontiera: lista di nodi in attesa di essere espansi (FIFO, LIFO, Priority, ...))

#### STRATEGIE NON INFORMATE

- **BF** (BreadthFirstSearch)
	- completa
	- ottima (se operatori con costo costante)
	- $T(b,d) = O(b^{d})$ (d := fattore di diramazione)
	- $S(b,d) = O(b^{d})$ (frontiera)
- **DF** (DepthFirstSearch)
	- ALBERO
		- non completa
		- non ottima
		- $T(b,m) = O(b^{m})$ (m := larghezza massima)
		- $S(b,m) = O(bm)$
	- GRAFO
		- per stati finiti: completa
		- per stati infiniti: non completa
		- $T(b,m) = O(b^{m})$
		- $S(b,m) = O(b^{m})$
  - RICORSIVA
    - non completa
    - non ottima
    - $S(b,m) = O(m)$
- **DL** (DepthFirstLimitedSearch)
	- se $d \le l$ completa (dovuto a limite di profondità)
	- se $d > l$ non completa
	- non ottima
	- $T(b,l) = O(b^{l})$ (l := limite)
	- $S(b,l) = O(bl)$
- **ID** (IterativeDeepingSearch)
	- compromesso tra BFS e DFS
	- se $b$ è finito: completa
	- se $b$ è infinito: non completa
	- se il costo del singolo passo è costante: ottima
	- se il costo del singolo passo è non costante: non ottima
	- $T(b,d) = O(b^{d})$
	- $S(b,d) = O(bd)$
- **UC** (UniformCostSearch - ChipestFirst)
	- se costo archi $\epsilon > 0$: completa
	- se costo archi $\epsilon \le 0$: non completa
	- se costo archi $\epsilon > 0$: ottima
	- se costo archi $\epsilon \le 0$: non ottima
	- $T(C,\epsilon) = O(b^{1+\lfloor C/ \epsilon \rfloor })$
	- $S(C,\epsilon) = O(b^{1+\lfloor C/ \epsilon \rfloor })$
- **BiDirectionalSearch**
	- completa
	- $T(b,d) = O(b^{d/2})$
	- $S(b,d) = O(b^{d/2})$


#### STRATEGIE INFORMATE (Ricerca Euristica)

Si usa conoscenza del problema ed esperienza per scegliere i cammini. La conoscenza del problema viene presa dalla stima del costo futuro usando la *funzione euristica* $h(n)$, cioè il costo (presumibile) da $n$ a $goal$.\newline
La scelta del cammino migliore viene fatta usando il costo dalla $root$ al nodo $n$ (già percorso e quindi costo sicuro) e dal nodo $n$ al $goal$ (non ancora percorso e quindi costo non sicuro).\newline
La stima totale del path verrà calcolata, quindi, dalla funzione 
$$f(n) = g(n) + h(n)$$

Un'euristica ammissibile è ottenibile attraverso:

- rilassamento del problema
	- approssimazione di un problema $\implies$ eliminazione di vincoli dal problema originario.
- massimizzazione di euristiche
	- data una serie di euristiche ammissibili si sceglie quella definita da\newline $h(n) = \max_{i=1}^{n}(h_i)$\newline che dominerà le altre
- database di pattern disgiunti
	- si calcola l'euristica ammissibile per ogni stato completo trovato durante una ricerca, somma dei costi dei sottoproblemi con l'eliminazione del costo delle sotto mosse che contribuiscono in altri sottoproblemi.
- combinazione lineare
	- somma di euristiche diverse
- apprendimento dall'esperienza
	- eseguire l'algoritmo cercando di apprendere come predire l'euristica
\newline

- **BestFirst**
	- simile ad UCS ma con uso della stima del costo per la coda di priorità
	- $f(n) = g(n) \equiv$ UCS
	- $f(n) = h(n) \equiv$ GreedyBestFirst
	- in generale: non completa
	- in spazi finiti con controllo di ripetizione: completa
	- non ottima
	- $T(b,m) = O(b^m)$
	- $S(b,m) = O(b^m)$
- **A**
	- algoritmo BestFirst con con funzione di valutazione del tipo: \newline
	$[f(n) = g(n) + h(n)] \land h(n)\ge 0 \land h(goal)=0$
	- completo $\implies g(n) \ge d(n) * \epsilon$ ($d(n)$ profondità del nodo $n$, $\epsilon$ costo minimo dell'arco) $\Longleftarrow$ se $n'$ è un nodo sul cammino soluzione allora verrà sicuramente espanso, $\implies$ essendo il $goal$ sul cammino verrà trovato sicuramente
- **A$^{\ast}$**
	- A con funzione $h(n)$ ammissibile: ammissibile $\iff \forall n . h(n) \le h^{*}(n)$ \newline ($h(n)$ è una sottostima e $h^{*}$ è il costo del cammino minimo da $n$ a $goal$)
	- completo $\Longleftarrow$ estensione di A, che è completo
	- ottimo
		- $h(n)$ sottostima $\implies$ possibile lavoro extra ma sicuro cammino migliore
		- $h(n)$ sovrastima $\implies$ probabilmente si perde il percorso migliore
		- **su alberi l'euristica ammissibile garantisce l'ottimalità**
		- **su grafi per garantire l'ottimalità serve garantire la consistenza (monotonicità) dell'euristica**
			- **consistenza**: \newline
			*Dato* $h(goal) = 0$ *e* $\forall n \, . \, h(n') \le c(n,a,n') + h(n) \implies f(n) \le f(n')$ \newline
			(condizione di Bellman: garantisce che soluzione meno costosa viene trovata per prima.)
			- monotona $\implies$ ammissibile 
			- ammissibile $\;\not\!\!\!\implies$ monotona
	- ottimamente efficiente
	- espande:
		- tutti i nodi con $f(n)<C^*$ ($C^*$ cammino ottimo)
		- alcuni nodi con $f(n)=C^*$ ($C^*$ cammino ottimo)
		- nessun nodo con $f(n)>C^*$ ($C^*$ cammino ottimo)
	- a parità di euristiche: quella più informata **è almeno** efficiente quanto quella meno informata
- **BeamSearch**
	- tiene in memoria solo i *k* nodi più promettenti, dove *k* è l'ampiezza del raggio di visita
	- non completo
	- non ottimo
- **IDA$^{\ast}$**:
	- combina A$^{\ast}$ con ID
	- La parte critica è l'aumento del limite
  	- costo variabile: ad ogni passo il limite successivo è dato dal valore minimo delle $f$ scartate in precedenza
    - costo fisso: incremento pari al costo delle azioni
	- completo
	- ottimale
- **RBFS** (RecursiveBestFirstSearch):
	- la ricerca si ferma quando viene trovato un nodo meno promettente, tornando indietro si salva il nodo migliore per un futuro utilizzo 
		- ad ogni iterazione tiene traccia del migliore percorso alternativo. 
	- Usa poca memoria (lineare nella profondità) ma compie molto lavoro.
- **SMA$^{\ast}$**:
	- struttura di A$^{\ast}$ ma quando memoria finita elimina il nodo peggiore aggiornandone il padre. 
		- A parità di $f$ sceglie il nodo migliore più recente e dimentica il più vecchio peggiore
	- se cammino ottimale in memoria allora ottimo

#### RICERCHE LOCALI (strategie informate)

Algoritmi in cui non è necessario ricordarsi il cammino, si tiene traccia solo del nodo attuale, questo comporta efficienza in spazio e all'aumento dello spazio di ricerca.\newline
Questi algoritimi sono utili per problemi di ottimizzazione, ricerca del punto più basso per la minimizzazione, ricerca del punto più alto per la massimizzazione, esplorazione eseguita in un ambiente variegato.\newline
Se esiste trovano sempre un obiettivo quindi sono completi

- **HillClimbing**
	- algoritmo greedy
	- generati i nodi immediatamente successori e valutati
	- scelto il nodo che migliora la situazione attuale
    - migliore: **HillClimbing a salita rapida**
    - random: **HillClimbing Stocastico**
    - primo: **HillClimbing a prima scelta**
	- se non esistono stati migliori terminazione con fallimento
	- Per *migliorare* il 'greedy':
		- consentire mosse laterali 
		- HillClimbing stocastico: scelta a caso tra le mosse in salita
			- lento ma soluzione migliore 
		- HillClimbing a prima scelta: generazione di mosse a caso fino a trovare la migliorare
			- efficace se molti successori 
		- HillClimbing con riavvio casuale: probabilità di successo = *p* allora servono in media $1/p$ riavii
			- quasi sempre completo $\implies$ se si insiste si generano tutte le mosse
- **TempraSimulata**
	- ad ogni passo un successore random
    - se migliora lo stato allora viene espanso
    - se non migliora allora nodo scelto con probabilità $p=e^{\Delta E/T}$
        - *p* inversamente proporzionale al peggioramento
        - *T* decresce con l'avanzamento dell'algoritmo secondo un piano definito\newline $\implies$ rende improbabili le mosse peggiorative
	- la probabilità di una mossa in discesa diminuisce con l'avanzamento\newline $\implies$ sempre più simile a comportamento di HillClimbing
	-   con *T* decrementato lentamente si ottiene soluzione ottimale
	-   $T_{iniziale}$ e decremento sono parametri
- **LocalBeam**
	- versione locale della BeamSearch
	- si tengono in memoria *k* stati
	- ad ogni passo si generano i successori dei *k* stati
    -   se si trova il goal $\implies$ STOP
    -   altrimenti si prosegue con i migliori *k* successori
- **BeamSearchStocastica** (algoritmo genetico)
	- si scelgono i *k* successori casualmente ma con una probabilità maggiore per i migliori
	- per i k stati generati casualmente
		- ogni stato valutato da una funzione *fitness*
		- si selezionano coppie di stati per il mixing con una probabilità proporzionale alla fitness
		- ogni coppia genera un successore che dovrebbe essere migliore dei genitori.

```{r echo=FALSE, fig.width=2.5, fig.height=3, fig.align='center'}
    y <- function(x) exp(sin(x)^3)/exp(cos(x))
    plot(y, 0, 5, xlab = "", ylab = "", axes = FALSE, sub="Esempio HillClimbing", cex.sub=.6)
```

##### Spazi continui

Su spazi continui un metodo che ci viene in aiuto, **se la $f$ è continua e derivabile**, è quello di usare il gradiente: $\nabla f(\bf{x})$ \newline (quantifica lo spostamento da effettuare senza cercarlo nello spazio inifinito)

$$\bf{x_{new}} = \bf{x_{t}} + \eta \nabla f(\bf{x})$$ 

(In ambiente parzialmente osservabile le percezioni sono importanti perché restringono gli stati possibili e l'agente è limitato solo nel decidere una strategia ma non un piano di lavoro che considera diverse eventulità)

- **Alberi AND-OR** 
	- nodo OR: scelte dell'agente 
	- nodo AND: stati possibili nell'ambiente $\rightarrow$ da considerare tutti
	- nodo obiettivo in ogni foglia 
	- unica azione nel nodo OR 
	- ha tutti gli archi uscenti da nodi AND

---

---

## Giochi con Avversario

Piani dell'agente devono considerare anche le mosse dell'altro agente.

- regole semplici e formalizzabili
- ambiente accessibile e deterministico
- ambiente multi-agente
- complessità e vincoli in tempo reale $\implies$ mossa migliore nel minor tempo possibile

### Algoritmi di risoluzione

#### Formulazione del problema

- stati: configurazioni di gioco $\implies$ *Player(s)*
- stato iniziale: configurazione iniziale di gioco
- *Actions(s)*: mosse legali in *s*
- *Result(s,a)*: stato *s'* risulto di una mossa
- *Terminal-Test(s)*: controlla terminazione del gioco
- *Utility(s,p)*: funzione di utilità $\implies$ valuta gli stati terminali

#### Algoritmi

- **MINIMAX**
	- *Min-Max* Metodo per minimizzare la massima perdita / massimizzare il minimo guadagno
	- Usato per i giochi a somma zero (guadagni e perdite sono bilanciati tra i giocatori: la somma dei valori dei partecipanti è pari a 0).
	- misura la 'bontà' di ogni stato del gioco
		- da una valore di bontà di quello stato per il giocatore
		- i giocatori che devono massimizzare il valore, sceglieranno le mosse che massimizzano la 'bontà' dello stato
		- i giocatori che devono minimizzare il valore, sceglieranno le mosse che minimizzano la 'bontà' dello stato
	- $T(b,m) = O(b^{m})$
	- $S(b,m) = O(m)$
	- per giochi non semplici impossibile esplorazione sistematica, necessarie euristiche
		- si usa una funzione di valutazione dello stato: *Eval(s)*
			- guardare avanti di *d* livelli per propagare poi indietro il risultato usando la regola del *Min-Max*
		- *Eval(s)*
			- stima della utilità attesa da una posizione
			- deve essere consistente con l'utilità se applicata a stati terminali
			- deve essere efficiente
			- deve essere congruente con le probabilità di vincita
			- valore atteso: combinazione della probabilità con utilità dello stato terminale (appreso da esperienza)
			- funzione applicata a stati quiescenti
			- uso di mosse che spostano il problema temporalmente in avanti (mosse inutili al fine della terminazione del gioco) $\implies$ **effetto orizzonte**

![Minimax tree](assets/minimax_tree.png){width=33%}

- **ALPHA-BETA PRUNING**
	- tecnica di potatura per la riduzione degli spazi di ricerca (applicabile a *MINIMAX*)
	- preso un nodo $\vartheta$, se esiste sopra di lui una scelta migliore, allora $\vartheta$ non sarà mai esplorato (l'algoritmo sceglierà l'opzione (sopra) migliore)
    - si procede fino a livello *d*
    - quando si propagano indietro i valori si decide se abbandonare il sotto-albero o meno.
    - si pota quando
    	- **sul nodo MAX**: $\vartheta \ge \beta$ (si taglia per bound di $\beta$)
    	- **sul nodo MIN**: $\vartheta \le \alpha$ (si taglia per bound di $\alpha$)\newline 
			**NB**: sui nodi *MAX* si lavora sui valori di $\alpha$, sui nodi *MIN* si lavora sui valori di $\beta$)
	- *miglioramenti*:
		- potatura ottimale si ha quando ogni livello è ordinato sulla qualità delle mosse. (le migliori prima)
  	  -   $T(b,m) = O(b^{m/2}) \implies$ profondità doppia
		- potatura in avanti esplorando solo mosse più promettenti
		- database di aperture e chiusure

![Alpha-Beta](assets/alphabeta.jpg){width=33%}


---

---

## CSP: Constraint Satisfaction Problems

Esempio di problemi con rappresentazione dello stato fattorizzata

### Modo di procedere

Ricerca in CSP significa assegnare una variabile ad ogni passo, quindi la massima profondità è data dal numero delle variabili. La risoluzione è quindi affidata ad euristiche specifiche per questa classe di problemi, permettendo di fare delle inferenze che limitano la ricerca o fare backtracking _intelligente_

#### Formulazione del problema

- stati: assegnamento parziale o completo di valori a variabili $\{x_{i} = v_{i}, x_{j} = v_{j}, ...\}$
- stato iniziale: $\{\}$
- azioni: assegnamento di un valore (lecito) ad una variabile
- soluzione (_goal_): assegnamento completo e consistente

Il problema è descritto dalle componenti:

- $X$: insieme di variabili
- $D$: insieme di domini
- $C$: insieme di vincoli

---

### STRATEGIE DI RICERCA NON INFORMATE

- **naive**
	- ad ogni passo si assegna una variabile, quindi profondità massima data dal numero di variabili
		- data l'ampiezza dello spazio di ricerca $\prod_{i=0}^{n}|D_{i}|$ con $|D_{i}|$ la cardinalità del dominio di $X_{i}$\newline
		$\implies$ il numero di foglie sarà $n! \cdot d^{n}$
	- diminuisce molto lo spazio di ricerca se il _goal_ è commutativo (l'ordine non importa)
		- numero di foglie sarà $d^{n}$
	- completo
- **DL con backtracking**
	- controllo anticipato della violazione dei vincoli
	- ricerca limitata in ogni caso dal numero delle variabili
	- completo
	- **_SELECT-UNASSIGNED-VARIABLE_**: scelta delle variabili
		- **MRV** (MinimumRemaningValues): scelta della variabile con meno valori legali $\implies$ variabile più vincolata
		- **Euristica del grado**: scelta della variabile con più vincoli con altre variabili
	- **_ORDER-DOMAIN-VALUES_**: scelta del valore
		- scelta del valore meno vincolante $\implies$ quello che esclude meno valori per le altre variabili
	- **_INFERENCE_**: propagazione dei vincoli
		- **Verifica in avanti** (FC): dopo assegnamento di un valore ad una variabile eliminazione dei valori incompatibili sulle altre
		- **Consistenza del nodo e arco**: si restringono i valori dei domini delle variabili considerando i vincoli su tutto il grafo
	- **_BACKTRACKING_**
		- **Cronologico**: si torna all'ultimo assegnamento che ha generato l'errore, provando tutti gli altri valori
		- **Intelligente**: si considerano i valori alternativi solo per le variabili che hanno causato fallimento $\implies$ guidato dalle dipendenze

#### Codici

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwBlock{BacktrackingSearch}{begin}{end}
\SetKwInOut{Output}{Output}
\SetAlgoNoLine
\SetAlgoNoEnd
\BlankLine
\SetKwFunction{FMain}{BACKTRACKING-SEARCH}
\SetKwProg{Fn}{}{}{}
\Fn{\FMain{esp}}{
			\KwRet{Backtrack(\{\}, esp)}
}
\;

\SetKwFunction{FMainn}{BACKTRACKING}
\SetKwProg{Fn}{}{}{}
\Fn{\FMainn{assignment, esp}}{
		\If{assignment.complete}{\KwRet{assignment}}
		var = SELECTED-UNASSIGNED-VARIABLE(esp)

		\ForEach{value in ORDER-DOMAIN-VALUES(var, assignment,esp)}
		{
				\If{value is consistent with assignment}
				{
					assignment.add(var=value)
					
					inferences = INFERENCE(esp, var, value)
					
					\If{inferences $\neq$ failure}
					{ 
						assignment.add(inferences)
					
						result=BACKTRACKING(assignment, esp)
					}

					\If{result $\neq$ failure}
					{
						\KwRet{result}
					}
					assignment.remove(var=value)
					assignment.remove(inverences)
				}
		}
	}
\Return{failure}
\caption{BACKTRACKING RICORSIVO}
\end{algorithm}

---

### STRATEGIE DI RICERCA INFORMATE

- **Riparazione Euristica**
	- si parte dall'assegnamento totale
	- ad ogni passo si modifica un assegnamento (per una variabile) che viola un vincolo
- **Min-Conflicts**
	- strategia locale con utilizzo di euristica dei _conflitti minimi_
		- si sceglie il valore che crea meno conflitti

---

---

\newpage

# Rappresentazione della Conoscenza e Ragionamento

## Agenti Basati su Conoscenza

Gli agenti basati su conoscenza hanno al loro interno una _base di conoscenza_ espressa in forma esplicita e dichiarativa.

In questo tipo di problemi:

- mondo complesso
	- rappresentazione parziale
	- rappresentazione incompleta
- linguaggi di rappresentazione della conoscenza
	- espressivi
	- inferenziali

*Knowledge Base* contiene tutta la conoscenza dell'agente in forma dichiarativa, in modo che l'agente possa essere più flessibile e possa modificare il comportamento con l'esperienza \newline
Alternativamente, la _Knowledge Base_ può essere codificata in modo precedurale con un programma per poter effettuare un processo decisionale.

### Formulazione della KB

- insieme di enunciati
- interazione attraverso interfaccia *'Tell-Ask'*
	- Tell $:=$ aggiungere enunciati
	- Ask $:=$ query
	- Retract $:=$ drop di un enunciato
- risposte discendenti dalla KB: $KB \models \alpha$ ($\alpha$ conseguenza logica di $KB$)

---

---

## Calcolo Proposizionale

- **Interpretazione**: definisce valore di verità per tutti i simboli proposizionali
- **Modello**: è un'interpretazione che rende vero un'insieme di formule

Una formula $\alpha$ è _conseguenza logica_ di una _KB_ se e solo se in ogni modello di _KB_ anche $\alpha$ è vera.\newline
Detto $M(\alpha)$ un insieme delle interpretazioni che rendono $\alpha$ vera e $M(KB)$ i modelli dell'insieme di formule in KB allora

$$KB \models \alpha \iff M(KB) \subseteq M(\alpha)$$

- **Equivalenza Logica**
	- $A \equiv B \iff A \models B \land B \models A$
- **Validità**
	- *A* è valida $\iff$ è vera in tutte le interpretazioni
		- **TAUTOLOGIA**
- **Soddisfacibilità**
	- *A* è soddisfacibile $\iff$ esiste una interpretazione in cui *A* è vera
		- *A* è valida $\iff$ $\neg A$ è insoddisfacibile

### Model Checking

Forma di inferenza che fa riferimento alla definizione di conseguenza logica (tecnica delle tabelle di verità).

### Algoritmi per la Soddisfacibilità

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Teorema di refutazione}}]
\emph{$KB \models A \iff (KB_{\land} \land \neg A)$ è insoddisfacibile}
\end{tcolorbox}

> 

- **TT-ENTAILS**
	- enumera tutte le possibili interpretazioni della _KB_
		- con *k* simboli $2^{k}$ interpretazioni possibili
	- per ogni interpretazione
		- se non soddisfa _KB_ $\implies \surd$ modello 
		- se soddisfa _KB_ $\implies$ necessario controllare anche $\alpha$
			- anche solo UNA interpretazione che soddisfa _KB_ ma NON $\alpha \implies \times$ modello
- algoritmi SAT
	- usano _KB_ in forma a clausole normale congiuntiva (CNF)
		- insiemi di letterali
			- ogni insieme rappresenta una disgiunzione logica ($\lor$)
			- gli insiemi sono legati da congiunzioni logiche ($\land$)
- **DPLL**
	- parte da _KB_ in forma a clausole
	- enumera in profondità tutte le possibili interpretazioni per trovare un modello
	- **terminazione anticipata**
		- decisione di verità su clausola anche su interpretazioni parziali
			- anche una sola clausola falsa, interpretazione non è un modello
	- **euristica dei simboli puri**
		- simbolo che appare con solito segno in tutte le clausole
			- letterale positivo $\implies$ TRUE; letterale negato $\implies$ FALSE
	- **euristica delle clausole unitarie**
		- clausola con un solo letterale NON ASSEGNATO
			- letterale positivo $\implies$ TRUE; letterale negato $\implies$ FALSE
	- Completo

### Algoritmi locali per la Soddisfacibilità

Gli stati sono assegnamenti completi, il goal è l'assegnamento che soddisfa tutte le clausole e quindi è un modello. Come stato iniziale si prende un assegnamento casuale e ad ogni passo si cambia il valore di una proposizione (FLIP). Gli stati sono valutati contando il numero di clausole non soddisfatte.

In questo tipo di algoritmi è necessario introdurre _perturbazioni casuali_ per poter evitare il più possibile i minimi locali.

- **WalkSAT**
	- ad ogni passo seglie:
		- a caso una clausola non ancora soddisfatta
		- sceglie il simbolo da modificare (con probabilità _p_ tra le scelte sotto)
			- passo casuale: scglie a caso il simbolo
			- passo ottimizzato: sceglie il simbolo che rende più clausole soddisfatte
	- si arrende dopo _F_ flip
	- se $maxflip = \infty$ prima o poi termina
	- ok per cercare modello ma non per verificarne l'esistenza
	- se problema molte soluzioni potrebbe trovarne una in poco tempo
	- rapporto tra clausole e simboli
		- più è grande più problema è vincolato, meglio si comporta _WalkSAT_
	- NON COMPLETO

### Inferenza come deduzione

La deduzione è un metodo per verificare se $KB \models \alpha$. Partendo da $KB \vdash \alpha$ ed usando le regole di inferenza, dovrebbero derivare tutte e sole le conseguenze logiche.

- _CORRETTEZZA_: $KB \vdash \alpha \implies KB \models \alpha$
- _COMPLETEZZA_: $KB \models \alpha \implies KB \vdash \alpha$

Decidere quale regola d'inferenza usare ad ogni passo significa esplorare lo spazio degli stati. Procedere in questo modo significa **dimostrare**. Per poter dimostare però sono necessari:

- _direzione_: all'indietro (`from goal to root`)
- _strategia_: problema decidibile ma comunque NP-Completo


--- 

---

## Logica del Primo Ordine

Nella logica del primo ordine si hanno assunzioni ontologiche più ricche:

- oggetti
- proprietà
- relazioni

### Regole di Inferenza

- **VARIABILI LEGATE**
	- Sono variabile che vengono usate nell'ambito dei quantificatori
- **VARIABILI LIBERE**
	- Variabili senza l'influenza dei quantificatori
- **FORMULA CHIUSA**
	- Formula che non contiene occorrenze di variabili libere
- **FORMULA GROUND**
	- formula che non contiene variabili

*Semantica dichiarativa*: stabilire relazione tra termini del linguaggi e oggetti del mondo, formule chiuse e valori di verità

- Eliminazione $\forall$
	- $\frac{\forall x A[x]}{A[g]}$
- Eliminazione $\exists$
	- $\frac{\exists x A[x]}{A[k]}$
	- se _x_ è legata anche a $\forall$, si usa una funzione $f$; se _x_ non è legata ad altri quantificatori si usa una costante $k$
		- $k$ e $f$ vengono dette **costante** e **funzione di Skolem**

#### Teorema di Herbrand

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Teorema di Herbrand}}]
\emph{Se $KB \models \alpha$, allora c'è una dimostrazione che coinvolge solo un sottoinsieme finito della $KB$ proposizionalizzata}
\end{tcolorbox}

Il modo di procedere è incrementale ed è il seguente:

1. Creazione di istanze con costanti
2. Creazione di istanze con 1° livello di annidamento
3. Creazione di istanze con 2° livello di annidamento
4. ...

Se $KB \not\models \alpha$ il processo _non_ termina, di conseguenza il problema è semidecidibile.

---

### Trasformazione a Clausole

La forma a clausole nel FOL ha la particolarità che vengono esclusi dalla definizione della forma a clausole del PROP le formule atomice $t_1 = t_2$.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Teorema di soddisfacibilità}}]
\emph{Per ogni formula chiusa $\alpha$ (nel FOL), è possibile trovare in maniera effettiva un insieme di clausole $FC(\alpha)$ soddisfacibile se e solo se $\alpha$ è soddisfacibile}
\end{tcolorbox}

1. **Eliminazione** delle implicazioni (ed equivalenze)
2. **Negazione** all'interno $\implies$ DeMorgan
3. **Standardizzazione** delle variabili (per ogni quantificatore una variabile diversa)
4. **Skolemizzazione**
5. **Eliminazione quantificatori universali**
6. **Forma normale congiuntiva**
7. **Notazione a clausole**
8. **Separazione delle variabili** uguale := diverse per ogni clausola
9. **Unificazione**: operazione per determinare se due espressioni possono essere rese identiche mediante una sostituzione di termini a variabili
10. **Sostituzione**: insieme finito di associazioni tra variabili e termini, in cui ogni variabile compare una sola volta sulla sinistra

#### Unificazione

Le espressioni sono unificabili se esiste un unificatore, cioè una sostituzione che le rende identiche. Tale unificatore non è unico, ma il più generale è il _**MGU**_

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Teorema di unificatore}}]
\emph{L'unificatore più generale è unico a meno dei nomi delle variabili (e l'ordine non conta)}
\end{tcolorbox}

L'algoritmo di unificazione esplora in parallelo due espressioni e costruisce l'unificatore. Fallisce l'unificazione appena vengono trovate due espressioni non unificabili. Causa più frequente di fallimento è l'impossibilità di unificare $x=f(x)$. Tale tipo di controllo viene detto _**occur-check**_

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Input{$x$, $y$, $\theta$}
\Output{sostituzione}
\SetAlgoNoLine
\SetAlgoNoEnd
\BlankLine
\If{$\theta$ == FAIL}{\Return{FAIL}}
\uElseIf{$x = y$}{\Return{$\theta$}}
\uElseIf{VARIABILE?($x$)}{\Return{UNIFY-VAR($x$, $y$, $\theta$)}}
\uElseIf{VARIABILE?($y$)}{\Return{UNIFY-VAR($x$, $y$, $\theta$)}}
\uElseIf{COMPUND?($x$) \& COMPUND?($y$)}{\Return{UNIFY($x$.ARGS, $y$.ARGS, UNIFY($x$.OP, $y$.OP, $\theta$))}}
\uElseIf{LIST?($x$) \& LIST?($y$)}{\Return{UNIFY($x$.REST, $y$.REST, UNIFY($x$.FIRST, $y$.FIRST, $\theta$))}}
\Else{\Return{FAIL}}
\caption{UNIFY}
\end{algorithm}

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Input{vat, $x$, $\theta$}
\Output{sostituzione}
\SetAlgoNoLine
\SetAlgoNoEnd
\BlankLine
\If{(var/val) in $\theta$}{\Return{UNIFY(val, $x$, $\theta$)}}
\uElseIf{($x$/val) in $\theta$}{\Return{UNIFY(var, val, $\theta$)}}
\uElseIf{OCCUR-CHECK?(var, $x$)}{\Return{FAIL}}
\Else{\Return{EXTEND(\{var/$x$\}, $\theta$)}}
\caption{UNIFY-VAR}
\end{algorithm}


#### Risoluzione per refutazione

$\Gamma \cup \{\neg A\}$ è insoddisfacibile $\iff \Gamma \models A$

$\Gamma$ è insoddisfacibile $\iff \Gamma \vdash \{\}$

---

### Strategie di risoluzione

- **CANCELLAZIONE**
	- letterali puri
	- tautologie: letterali identici e complementari in clausola
	- clausole sussunte: implicate
- **RESTRIZIONE**
	- risoluzione unitaria
		- almeno una delle due clausole è unitaria
		- se clausole Horn (con al più un letterale positivo): completo
		- in generale: non completo
	- risuoluzione lineare
		- ultima clausola generata con una clausola di input o un antenata
		- completo per refutazione
	- risoluzione guidata dal goal
		- dato un insieme di supporto ($\subset$ _KB_) per insoddisfacibilità
		- almeno una delle due clausole appartiene a questo insieme (o sottoinsiemi)
		- tipicapente insieme di supporto iniziale è il _goal_ negato
		- completo per refutazione
- **ORDINAMENTO**
	- ogni clausola è un insieme ordinato di letterali e si possono unificare solo i primi
	- se clausole Horn: completo
	- in generale: non completo

---

---

\newpage

# Introduzione al Machine Learning

> _L'apprendimento è probabilmente il vero centro del problema dell'intelligenza_ [Poggio, 1999]

- **ML usato perché**
	- c'è maggior bisogno di analisi dei dati
	- si cambia il paradigma delle scienze, dove si viene guidati dai dati: paradigma _data-driven_
	- è più utile che la macchina impari autonomamente, in quanto è difficile fornire la macchiana di intelligeza usando solo la programmazione 
- **ML viene usato come**:
	- _metodologia per la IA_: creazione di sistemi intelligenti adattivi
	- _per apprendimento statistico_: creare un sistema predittivo potente per l'analisi intelligente dei dati 
	- _come strumento informatico per aree applicative innovative_: usando modelli come uno strumento per problemi complessi (interdisciplinari)
- **ML usato quando**: (opportunamente e consapevolmente **se utile e se necessario e in casi limitati**)
	- _utilità di modelli di apprentimento predittivo_:
		- senza o con poca teoria per spiegare il fenomento, quindi difficile formalizzazione
		- dati incerti, rumorosi, incompleti
		- ambienti dinamici (componenti personalizzati) o non conosciuti a priori
	- _requisiti_:
		- risorse per esperienza formativa
		- tolleranza sulla precisione dei risultati
- **ML usato per**:
	- trovare una soluzione approssimativa di un problma difficile o difficile da formalizzare con un algoritmo "fatto a mano"
	- costruire un sistema intelligente robuso e largamente applicabile

**ML non un metodo approssimativo, ma metodo rigoroso per trovare funzioni approssimate per problemi difficili**

Nel machine learning, fare apprendimento significa aprossimare una funzione _NON_ nota, andando ad adattarla a dati presi da esempi noti.

\begin{tcolorbox}[coltitle=black!70!white, coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Modello}}]
\emph{Definisce la classe delle funzioni che può implementare la macchina in apprendimento, come ad esempio un set di funzioni $h(x,w)$ dove $w$ è il parametro\newline
Il modello ha lo scopo di catturare e descrivere relazioni tra i dati}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Esempi di Training}}]
\emph{Esempio di input ed output della forma $(x,f(x))$ dove $x$ è un vettore di caratteristiche e $f(x)$ è detto valore target}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Funzione Target}}]
\emph{La funzione $f$, di riferimento per la correttezza di predizione del modello}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Ipotesi}}]
\emph{Funzioni (proposte) $h$ che si ritengono simili a $f$ descritte in un linguaggio $L$}
\end{tcolorbox}


Il linguaggio $L$, con cui le ipotesi sono scritte, può essere

- logica del primo ordine
- equazioni matematiche
- probabilità

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Spazio delle Ipotesi}}]
\emph{Lo spazio di tutte le ipotesi che potrebbero essere l'output dell'algoritmo di learning}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Algoritmo di Learning}}]
\emph{Algoritmo basato su dati, task e modello che ricerca attraverso lo spazio delle ipotesi $H$ le migliori ipotesi, cioè quelle ipotesi che aprossimano meglio la funzione target $f$}
\end{tcolorbox}

Fasi del machine learning:

1. _Fase di learning_: creazione del modello partendo dai dati conosciuti
	- Training
	- Fitting
2. _Fase predittiva_: applicazione a nuovi esempi, valutando le ipotesi predittive
	- Test
	- La performance dell'algoritmo corrisponde alla sua accuratezza predittiva


## Supervised Learning

1. classificazione
2. regressione

$f(x)$: $x \rightarrow$ categorie o valori reali (dove $x$ è lo spazio di input (vettori))

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Supervised Learning}}]
\emph{Task del machine learning per imparare (costruire) una funzione che mappi un valore di input su un valore di output basandosi su esempi di coppie $<input, output>$}
\end{tcolorbox}

**Problema:**\newline
_DATI_ gli esempi di training, $<input, output>=<x,d>$, per una funzione $f$ sconosciuta _TROVARE_ una buona approssimazione di $f$

Il valore *target d* (t, y, ...) è un'etichetta categorica o numerica data dall'insegnate

-  _classificazione_: la funzione $f(x)$ restituisce la classe assunta corretta per $x$ ($f(x)$ è a valori discreti) 
	- separazione dei dati in due o più classi
- _regressione_: la funzione $f(x)$ restituisce valori reali continui che approssimano una funzione target, a valori reali ($\mathbb{R}$ o $\mathbb{R}^k$), su esempi di training rumorosi
	- interpolazione dei dati per associargli una o più caratteristiche

### Concept learning

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Concept Learining}}]
\emph{Inferisce il valore di una funzione booleana partendo da training examples positivi e negativi}
$$C : X \rightarrow \{TRUE,FALSE\}$$
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Soddisfacibilità}}]
\emph{Un'ipotesi $h$ ($X \rightarrow \{TRUE,FALSE\}$) soddisfa $X$ se $h(x)=1$}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Consistenza}}]
\emph{Un'ipotesi è consistente con:}
\begin{itemize}
\item un esempio $<x,c(x)>$ se $h(x) = c(x)$
\item $D$ se $h(x) = c(x)$ per ogni esempio in $D$ ($D$ training set)
\end{itemize}
\end{tcolorbox}

Nel caso generale il numero delle ipotesi nello spazio delle ipotesi è $|H| = 2^{\#istanze} = 2^{2^{n}}$ per inputs e outputs binari, con $n$ la dimensione dell'input

#### Regole congiuntive

L'ipotesi $h$ è una congiunzione di vincoli sugli attributi e ogni vincolo può essere

- un valore specifico (\<*value*\>)
- un valore non importante (?)
- nessun valore consentito ($\emptyset$)

Considerando solo i letterali positivi, in questo caso, lo spazio delle ipotesi scende a $|H| = 2^n$ mentre considerando tutti i letterali (compresi i negati) lo spazio diventa $|H| = 3^{n}+1$

Un'ipotesi è specifica se è del tipo $<\emptyset,\emptyset,...,\emptyset>$. \newline Un'ipotesi è generale se è del tipo $<?,?,...,?>$

_Assunzione per questo tipo di learning_: \newline
Ogni ipotesi trovata che approssima bene la funzione target sui dati di esempio, approssimerà bene la funzione target anche su dati mai visti prima. \newline 
( $h(x) = c(x) \; \forall x \in D \implies h(x) = c(x) \; \forall x \in X$ )

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Generalità}}]
\emph{Dati $h_{j}$ e $h_{k}$ come funzioni a valori booleani definite su $X$, $h_{j}$ è più generale o generale quanto $h_{k}$ ($h_{j} \ge h_{k}$) se e solo se $\forall x \in X \; | \; (h_{k}(x)=1) \rightarrow (h_{j}(x)=1)$}
\end{tcolorbox}

La relazione $\ge$ impone un ordine parziale sullo spazio delle ipotesi che è utilizzato da molti metodi di concept learning, questo può essere sfruttato per organizzare efficientemente la ricerca in $H$

![Concept Learning](assets/conceptL){width=33%}

##### FIND-S

Sfrutta l'ordine parziale per cercare *h* , senza enumerare ogni ipotesi

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetAlgoNoLine
\SetAlgoNoEnd
\BlankLine
Inizializzazione di $h$ all'ipotesi più specifica in $H$

\ForEach{istanza di training $x$ positiva}
{
    \ForEach{attributo $a_{i} \in h$}
    {
        \eIf{$a_{i}$ è soddisfatto da $x$}{continue}
        {sostituzione di $a_{i}\in h$ con il vincolo più generale che è soddisfatto da $x$}
    }
}

\Return{$h$}
\caption{Find-S}
\end{algorithm}

Un modo per organizzare la ricerca è iniziare con le ipotesi più specifiche in $H$ per poi generalizzare ogni volta che non si riesce a coprire un esempio positivo osservato. Un'ipotesi copre un esempio positivo se classifica correttamente l'esempio come positivo. \newline 
Un'ipotesi è "buona" quando si adatta a più esempi positivi possibili.

**Proprietà**:

- Lo spazio delle ipotesi è descrito da congiunzioni di attributi
- Ritorna l'ipotesi più specifica in $H$ consistente con gli esempi di training positivi
	- questa ipotesi sarà consistente anche con gli esempi negativi ($\impliedby$ $c \ge h$)
- Impossibile dire se il *learner* ha converso sul target obiettivo in quanto non sa determinare se ha trovato l'unica ipotesi consistente con gli esempi di training.
- Impossibile dire quando i dati di allenamento sono incosistenti poiché ignora gli esempi di training negativi $\implies$ nessuna tolleranza al rumore.

##### Version Space

L'idea principale in questo tipo di algoritmi è quella di ottenere in _output_ una descrizione dell'insieme di tutte le ipotesi $h$ consistenti con $D$:

$$Consistent(h,D) := {<x,c(x)> \in D \; |\; h(x)=c(x)}$$

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white, title=Definizione \textbf{\emph{Version Space}}]
\emph{Il Version Space rispetto allo spazio delle ipotesi H e al training set D, è il sotto insieme delle ipotesi di $H$ consistenti con tutti gli esempi di training}
$$VS_{H,D} = \{h \in H | Consistent(h,D) \}$$
\end{tcolorbox}

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetAlgoNoLine
\SetAlgoNoEnd
Inizializzazione del \emph{Version Space} con la lista contenente ogni ipotesi di $H$
\ForEach{esempio di training}
{
    rimuovere da \emph{Version Space} qualsiasi ipotesi che non è consistente con l'esempio di training
}

\Return{ la lista delle ipotesi nel \emph{Version Space}}
\caption{List-Then Eliminate}
\end{algorithm}

##### Candidate Elimination

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{General Bound}}]
\emph{Il limite Generale $G$ del Version Space è l'insieme dei membri massimamente generali}
$$G = \{g \in H \; | \; Consistent(g,D) \; \land \; \nexists g' \in H \; | \; g' > g \lor Consistent(g',D)\}$$
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Specific Bound}}]
\emph{Il limite Specifico $S$ del Version Space è l'insieme dei membri massimamente specifici}
$$S = \{s \in H \; | \; Consistent(s,D) \; \land \; \nexists s' \in H \; | \; s' < s \lor Consistent(s',D)\}$$
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Teorema Version Space Bounds}}]
\emph{Ogni membro del Version Space si trova tra il limite Generale e il limite Specifico}
$$VS_{H,D} = \{h \in H \; | \; \exists s \in S, g \in G \; . \; g \ge h \ge s\}$$
$$S = \{s \in H \; | \; Consistent(s,D) \; \land \; \nexists s' \in H \; | \; s' < s \lor Consistent(s',D)\}$$
\end{tcolorbox}

 | 
- | -
![Esempi Positivi](assets/CEPos.png){width="33%"} | ![Esempi Negativi](assets/CENeg.png){width="33%"}


\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwInOut{Input}{Input}
\SetAlgoNoLine
\SetAlgoNoEnd
\Input{$G$, $S$}\tcp{$G$ = ipotesi più generale}
            \tcp{$S$ = ipotesi più specifica}
\BlankLine
\ForEach{$d \in$ TrainingExamples}
{
    \If{ $d$ == "positive" }
    {
        rimuovere da $G$ qualsiasi ipotesi non consistente con $d$
    
        \ForEach{$s \in S$ non consistente con $d$}
        {
            rimuovere $s$ da $S$

            aggiungere a $S$ tutte le minime generalizzazioni $h$ di $s$ tali che
                
                \begin{itemize}
                \item $h$ è consistente con $d$
                
                \item qualche membro di $G$ è più generale di $h$
                \end{itemize}
            
            rimuovere da $S$ qualsiasi ipotesi che è meno specifica (più generale) di un'altra ipotesi in $S$
        }
    }
    \If{ $d$ == "negative" }
    {
        rimuovere da $S$ qualsiasi ipotesi non consistente con $d$
    
        \ForEach{$s \in S$ non consistente con $d$}
        {
            rimuovere $g$ da $G$

            aggiungere a $G$ tutte le minime specializzazioni $h$ di $g$ tali che
                
                \begin{itemize}
                \item $h$ è consistente con $d$
                
                \item qualche membro di $S$ è più specifico di $h$
                \end{itemize}
            
            rimuovere da $G$ qualsiasi ipotesi che è meno specifica (più generale) di un'altra ipotesi in $G$
        }
    }
}
\caption{Candidate Elimination}
\end{algorithm}

##### Decision Tree

Rappresenta una disgiunzione di congiunzioni di vincoli sugli attributi: regola if-then-else.

Le ipotesi nel _Decision Tree_ sono capaci di esprimere qualsiasi funzione finita a valori discreti

##### ID3

ID3 è l'algoritmo base per l'apprendimento con Decision Tree.

Dato un training set di esempi, costruisce l'albero di decisione cercando nello spazio degli alberi di decisione. Esegue una ricerca *greedy* costruendo un albero Top-Down. Seleziato il miglior attributo al livello $l$, i nodi discendenti da tale nodo sono creati da ogni possibile soluzione dell'attributo del nodo a livello $l$ e gli esempi sono partizionati secondo questo valore. Il processo termina solo quando gli esempi sono classificati corretametne o non ci sono più attributi.

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwFunction{CreateNode}{CreateNode}
\SetAlgoNoLine
\SetAlgoNoEnd
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Input{X,T,Att}\tcp{X training examples}
                \tcp{T: target attribute}
                \tcp{Attrs: other attributes, initially all attributes}
\Output{root}\tcp{la radice dell'albero}
\BlankLine
\CreateNode{root}

\If{ all $X$'s are positive}
{
    \Return{root with class positive}
}
\If{ all X's are negative}
{
    \Return{root with class negative}
}
\eIf{Attrs is empty}
{ 
    \Return{root with class most common value of $T$ in $X$}
}
{ 
    $A \leftarrow$ best attribute
    
    decision attribute for root $\leftarrow A$

    \ForEach{possible value $v_{i}$ of $A$}
    {
        add a new branch below Root, for test $A = v_{i}$
        
        $X_{i} \leftarrow$ subset of $X$ with $A = v_{i}$

        \eIf{Xi is empty}{add a new leaf with class the most common value of T in X}
        {add the subtree generated by \emph{ID3( $X_{i}$, T, Attrs $\setminus \{A\}$ )}}
    }
}
\Return{root}

\caption{ID3}
\end{algorithm}

###### Entropia

L'_**entropia**_ misura il grado di impurità della collezione dei dati e dipende dalla distribuzione della variabile randomica $p$

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Entropia}}]
\emph{Dati $S$ la collezione di training examples, $p_{+}$ la proporzione di esempi positivi in $S$ e $p_{-}$ la proporzione degli esempi negativi in $S$. Si definisce entropia}
$$Entropy(S) = -p_{+}log_{2}p_{+}-p_{-}log_{2}p_{-}$$
\end{tcolorbox}

Esempi:

1. $Entropy([14+, 0-]) = -\frac{14}{14}log_{2}(\frac{14}{14}) - 0 log_{2}(0) = 0$ $\implies$ bassa entropia
2. $Entropy([7+, 7-]) = -\frac{7}{14}log_{2}(\frac{7}{14}) - -\frac{7}{14}log_{2}(\frac{7}{14}) = 1$ $\implies$ alta entropia

_NB_: $0\le p \le 1$ e $0\le Entropy \le 1$

![Entropy vs $p$](assets/entropy.png){width=33%}


###### Gain

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Gain}}]
\emph{Information Gain è la riduzione attesa nell'entropia causata dal partizionamento degli esempi su un attributo (conoscendo $A$)}
$$Gain(S,A) = Entropy(S) - \sum_{v\in Value(A)}\frac{|S_{v}|}{|S|}Entropy(S_{v})$$
\end{tcolorbox}

dove $Value(A)$ sono i possibili valori per $A$, $A$ è il nodo a cui si riferiscono i valori, $S_{v}$ il sottoinsieme di esempi di $S$ per i quali $A$ ha valore $v$. (Somma pesata)

**Più il valore del _Gain_ è alto, più efficace sarà l'attributo nella classificazione dei dati di training (quindi massima seprarazione delle classi).**

Si utilizza l'_Entropy_ per misurare l'omogeneità (invece che impurità) della classe del sottoinsieme degli esempi, quindi si seleziona quell'attributo $A$ che massimizza il _Gain_. \newline
L'obiettivo è quello di separare gli esempi sulla base del target, trovando l'attributo che discrimina gli esempi che appartengono a classi differenti. \newline 
(Es: Tutti i $-$ a sinistra, tutti i $+$ a destra)

###### Gain Ratio

L'_Information Gain_ favorisce attributi con più valori possibili. Prendendo ad esempio come un possibile attributo la _Data_, l'_information gain_ su questo attributo è massima in quanto ogni giorno appartiene ad un sottoinsieme differente generando la minima entropia possibile (0) e quindi avendo una perfetta separazione dei training data. Tutto questo però non è significativo in quanto non è utile su istanze mai viste.

Per ovviare il problema della generazione di molti piccoli sottoinsieme si introduce un'ulteriore misura detta _**Gain Ratio**_

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Gain Ratio}}]
$$GainRatio(S,A) = \frac{Gain(S,A)}{SplitInformation(S,A)}$$

$$SplitInformation(S,A) = -\sum_{i=1}^{c}\frac{|S_{i}|}{|S|}log_{2}\frac{|S_{i}|}{|S|}$$
\end{tcolorbox}

dove $S_{i}$ sono gli insiemi ottenuti dal partizionamento sui valori $v_{i}$ di $A$ (fino a $c$ valori)

_SplitInformation_ misura l'entropia di $S$ rispetto ai valori di $A$. Ciò significa che più i dati sono "uniformemente dispersi", più alta sarà l'entropia.\newline
_GainRatio_ penalizza, quindi, gli attributi che dividono gli esempi in tante piccole classi (esempio _Data_). Se però si capitasse nel caso in cui lo _SplitInformation_ è 0 (o un valore tendente a 0 ($\implies |S_{i}| \simeq |S|$)), il _GainRatio_ non da nessuna informazione ($\rightarrow \infty$).\newline
Per cercare di ovviare a questo problema, si aggiusta l'euristica in questo modo:

1. si calcola il _Gain_ per ogni attributo
2. si applica il _Gain Ratio_ solo agli attributi con _Gain_ oltre la media

La ricerca nello spazio delle ipotesi con **ID3** avviene attraverso una ricerca con _hill-climbing_ su tutto lo spazio di tutti i possibili _Decision Tree_, dal più semplice al più complesso.


![Geometrical view: Decision Tree divide l'input space in rettagoli con 1 delle K etichette (foglie)](assets/dt.png){width=33%}

---

##### Problemi e Soluzioni

Come già detto il _Decision Tree_ potrebbe aver problemi di overfitting, portando quindi a fermate premature, ridotta potatura degli errori ed introduzioni di regole per il post-pruning.\newline
Per alcuni di questi problemi ci sono delle soluzioni come ad esempio:

- misure alternative per la scelta dell'attributo
- attributi a valori nel continuo
- gestione degli esempi di training con valori di attributi mancanti
- gestione degli attributi con costi differenti
- miglioramento dell'efficienza computazionale

##### Overfitting

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Overfitting}}]
\emph{Ipotesi $h$ è in overfit sui dati di training se c'è un'ipotesi alternativa $h'$ tale che: }
$$[error_{D}(h) < error_{D}(h')] \land [error_{X}(h') < error_{X}(h)]$$\end{tcolorbox}


Significa costruire (ad esempio) l'albero che si adatta troppo agli esempi di training, quindi **$h'$ si comporta peggio sui dati del training set rispetto a dati non ancora visti** e questo è molto probabile che accada con modelli flessibili

Per mitigare l'overfitting:

-   fermare la crescita dell'albero prima della classificazione perfetta
-   permettere l'overfitting dei dati per poi andare a fare pruning sull'albero

##### Stopping

Fermare la crescita significa non esplorare alcuni sotto-alberi, ma per decidere quali di questi non esplorare sono necessarie delle linee guida.

Un metodo utile a bloccare la crescita è quello di dividere il _training set_ in due parti: 

- una (_validation set_) si usa per decidere quando fermarsi e l'altra per allenare l'algoritmo
- si usa una misura di complessità per individuare il punto in cui l'albero ha valore di questa misura maggiore, quello è il punto di stop. (Minimum Description Lenght)

##### Pruning

Potare consiste nel rimovere un sottoalbero radicato in un nodo *n*, tale nodo diventa una foglia e viene assegnata la classificazione più comune. Quindi ogni nodo è candidato per la potatura. I nodi sono effettivamente rimossi solo se l'albero risultate ha prestazioni non peggiori sull'insieme di validazione. La potatura avviene iterativamente fino a che non ne migliora l'accuratezza.

###### Regole di Post-Pruning

1. Creare il _Decision Tree_ sui dati del training set
2. Convertire l'albero in un set di regole equivalente
    * ogni path corrisponde ad una regola
    * ogni nodo su un path corrisponde a una pre-condizione
    * ogni foglia è un'etichetta
3. Potare (generalizzare) ogni regola rimuovendo le precondizioni migliorando così l'accuratezza su:
    - validation set
    - training con una pessimistica e staticamente ispirata misura euristica
4. Ordinare le regole basandosi sul grado di accuratezza e considerarle in sequenza quando classificano nuove istanze

Considerando che ogni path distinto produce una regola differente (eliminazione di una condizione può dipendere dal contensto) e che la potatura delle precondizioni è specifica delle regole (path), la potatura dei nodi è globale e "colpisce" tutte le regole, convertire le regole migliora la leggibilità per gli uomini (assumendo un numero limitato di regole)

---

##### Costo degli attributi

Gli attributi dell'istanza potrebbero avere un costo associato che vorremmo tenere di conto durante la scelta del Decision Tree.

Per questo tipo di problema sono stati introdotti due tipi di aggioramento all'algoritmo ID3

1. Tan & Schilmmer: $\frac{Gain^{2}(S,A)}{Cost(A)}$
2. Nunez: $\frac{2^{Gain(S,A)}-1}{[Cost(A)+1]^{w}}$ con $w \in [0,1]$

---

#### Bias

- _Lookup Table_: nessun bias
- _Candidate Elimination_: lo spazio delle ipotesi contiene i target concept (congiunzione di attributi)
- _Find-S_: lo spazio delle ipotesi contiene il target concept e tutte le istanze sono istanze negative a meno che il contrario sia implicato dalla sua conoscenza $\implies$ language bias, dovuto all'AND sui letterali, più search bias, dovuto alla preferenza delle ipotesi più specifiche.
- _Decision Tree_: sono preferiti alberi più corti,questo implica che essendo una ricerca dal caso più semplice a quello più complesso la ricerca diventa incrementale. Questo però non è ancora sufficiente per migliorare _ID3_ rispetto a _BFS_. Si preferiscono alberi che posizionano gli attributi con alto _Information Gain_ vicino alla radice.

_NB_: I Decision Tree non sono limitati sulla rappresentazione di tutte le funzioni, cioè non hanno restrizioni sullo spazio delle ipotesi ma sulla strategia di ricerca.

##### Bias Induttivo

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Bias}}]
\emph{Si assume che il target concept possa essere descritto da una congiunzione ($\land$) di letterali, limitando la capacità di rappresentazione}
\end{tcolorbox}

Nell'ipotesi di usare un _bias non vincolato_, cioè che $H$ ha la possibilità di esprimere ogni concetto insegnabile, allora, ogni istanza non osservata, il Version Space la classifica per metà delle ipotesi positive e per l'altra metà per le ipotesi negative. Tale learner si definisce **_unbiased_**, in quanto lo spazio è più espressivo della capacità di generalizzazione del learner. Questo può portare alla capacità di classificare solamente esempi già osservati e/o classificati.

> **Un unbiased learner è impossibilitato a generallizzare**

*Dimostrazione:*\newline
ogni istanza non osservata sarà classificata positiva da esattamente metà delle ipotesi nel Version Space e negativa dall'altra metà (rejection)\newline
$\forall h$ cosistente con $x_i$ (test), $\exists h'$ idetica a $h$ tranne che $h'(x_i) \not= h(x_i), h \in VS \implies h' \in VS$\newline

Un learner che non fa ipotesi a priori riguardo l'identità del target concept, non ha una base razionale per classificare qualsiasi istanza mai vista

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Bias Induttivo}}]
\emph{Un Bias Induttivo di $L$ è un qualsiasi insieme minimale di asserzioni $B$ tali che per qualsiasi target concept $c$ e i corrispondendi dati di training $D_{c}$ vale}
$$\forall x_{i} \in X \; . \; (B \land D_{c} \land x_{i}) \vdash L(x_{i}, D_{c})$$
\end{tcolorbox}

($A\vdash B$ significa $A$ implica logicamente $B$, (segue deduttivamente da))

Due tipi di _Bias_ utilizzabili possono essere

1. **Search Bias (Preference)**: bias legato alla strategia di ricerca
	- esempio: ID3 ricerca uno spazio delle ipotesi _completo_ $\implies$ la strategia di ricerca è _incompleta_
2.  **Language Bias (Restriction)**: bias legato alle ipotesi esprimibili o considerabili
	- esempio: Candidate-Elimination cerca uno spazio delle ipotesi _incompelto_ $\implies$ la strategia di ricerca è _completa_

Il _Search Bias_ può essere preferibile rispetto al _Language Bias_ perché in machine learning tipicamente si usano approcci flessibili senza escludere funzioni target a priori e quindi senza limitarne l'espressività.\newline
Più flessibilità significa anche però avere maggior rischio di overfitting.\newline
Il motivo del fatto che le ipotesi più corte sono preferibili si può capire usando il _**rasoio di Ockham**_ il quale dice:

> _«La spiegazione più semplice è (quasi) sempre quella corretta.»_

Ciò significa _"rasare"_ via le ipotesi superflue per arrivare alla spiegazione più semplice.

---

### Linear Model

L'approccio lineare è un approccio ben fondato in quanto la conoscenza è rappresentata compattamente ma con forti assunzioni sulle relazioni tra i dati.\newline
Il _LMS_ è un algoritmo di correzzione iterativa dell'errore che continuamente cerca nello spazio delle ipotesi.

![Esempio di **Decision Boundary** in 3D](assets/lmDB.png){width=33%}


#### Regressione

I vantaggi del modello lineare sono che se funziona nel modo corretto è un ottimo modello in quanto:

- semplice
- le informazioni sui dati possono essere rintracciate attraverso $w$
- semplice da interpretare ($\implies$ usato quotidianamente in molte discipline applicate)
- tollerabilità ai dati rumorosi
- ideali per creare una legge naturale (nel metodo scientifico)

Questo comporta che è una buona base per l'apprendimento ed infatti è usato (incluso) in molti modelli più complessi.

##### Univariate Linear Regression

Univariate Linear Regressione è il caso semplice della regressione lineare, in quanto si acquisisce una variabile in input e si restituisce una variabile (valore) in output.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Modello di regressione}}]
\emph{Si assume un modello $h_{w}(x)$ espresso come}
$$out = h(x) = w_{1}x+w_{0}$$
\end{tcolorbox}

dove $w$ sono coefficienti a valore reale detti anche parametri liberi o pesi.

L'adattamento dei dati avviene con una retta "banale" del tipo $y=mx+q$

**Problema:**\newline
_DATA_ la variabile $y$ che è linearmente correlata alla variabile $x$ (anchessa data) attraverso la formula $y = w_{1}x + w_{0} + noise$ e al rumore (con $w$ variabile libera), _TROVARE_ $h$ come modello lineare che si adatti meglio ai dati.

Il modello si costurisce trovando i valori $w$ per predire/stimare altri valori mai osservati prima.

##### Least Mean Square

Come apprendimento si intende trovare $w$ che minimizzi l'errore/perdita empirica $\implies$ miglior fitting sull'insieme di training con $l$ esempi.

**Problema:**\newline
_DATO_ un set $l$ di esempi di training _TROVARE_ $h_{w}(x)$ nella forma $w_{1}x+w_{0}$ che minimizza la _Loss_ ($E(w)$) aspettata sui dati di training

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Least Mean Square}}]
\emph{Trovare $w$ che minimizza la somma residua dei quadrati}
$$Loss(h_{w}) = E(w) = \sum_{p=1}^{l}(y_{p}-h_{w}(x_{p}))^{2} = \sum_{p=1}^{l}(y_{p}-(w_{p}x_{p}+w_{0}))^{2}$$
\end{tcolorbox}

dove la somma viene eseguita su ogni esempio $p$, $x_{p}$ è il $p$-esimo input, $y_{p}$ è l'output alla $p$-esima $x$, $w$ i parametri liberi e $l$ il numero di esempi. Per evitare valori negativi, senza incorrere nelle problematiche del valore assoluto, si vanno ad usar i quadrati all'interno della sommatoria.

![E(x)=LOSS - x,y=input,output - h=curva di fitting](assets/LMS.png){width="33%"}

Il metodo dei quadrati minimi è un approccio standard sulla risoluzione di sistemi in cui il numero di equazioni è maggiore delle incognite.

Per minimizzare la _Loss_ possiamo ricorrere al concetto di **_gradiente_**, dove in un punto stazionario, quindi minimo locale, il gradiente è nullo. In spazi su più dimensioni tale concetto si può definire dalla formula:

$$\frac{\partial E(w)}{\partial w_{i}} = 0 \; \; \forall i \in [1, dimensioneInput+1]$$

Quindi per la semplice regressione lineare in 2 parametri, la _Loss_ diventa

$$\frac{\partial E(w)}{\partial w_{0}} = -2(y-h_{w}(x)) = 0$$

$$\frac{\partial E(w)}{\partial w_{1}} = -2(y-h_{w}(x))x = 0$$

![Gradiente](assets/gradient.png){width="33%"}


##### Local Search

Sapendo che il gradiente su un punto di minimo locale è nullo, possiamo usarlo su ogni dato ricevuto per individuare la direzione da seguire per spostarci verso questo minimo.

Quindi la ricerca locale inizia il processo iterativo di discesa usando un vettore di pesi che verrà modificato iterativamente fino alla minimizzazione della funzione di errore. 

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Calcolo dei nuovi pesi}}]
$$w_{new} = w_{old} + \eta * \Delta w$$
\end{tcolorbox}

dove $\eta$ è una costante, la costante di apprendimento, che viene detta *learing rate*, mentre $\Delta w = -\nabla(E(w))$ corrisponde al passo da fare nella direzione indicata dal gradiente.

Questo approccio è una regola di "correzzione dell'errore" detta **_delta rule_** che cambia $w$ proporzionalmente all'errore:

-   $y_{p} - h(x_{p}) = 0 \implies$ nessun errore $\implies$ nessuna correzzione
-   $y_{p} - h(x_{p}) < 0 \implies$ l'output è troppo alto (target troppo basso) $\implies$ correzzione di $w_{0}$ e $w_{1}$ per *diminuire* il valore
-   $y_{p} - h(x_{p}) > 0 \implies$ l'output è troppo basso (target troppo alto) $\implies$ correzzione di $w_{0}$ e $w_{1}$ per *aumentare* il valore

Questo tipo di approccio consente di ricercare in uno spazio d'ipotesi infinito. Infatti, può essere sempre applicata per $H$ continui a perdita differenziale.

Nel caso si usino $l$ patterns $(x_{p}, y_{p})$ si ottiene:

-   $\Delta w_{0} = -\frac{\partial E(w)}{\partial w_{0}} = -2\sum_{p=1}^{l}(y-h_{w}(x)) = 0$
-   $\Delta w_{1} = -\frac{\partial E(w)}{\partial w_{1}} = -2\sum_{p=1}^{l}(y-h_{w}(x))x = 0$

A seconda anche di quando si decide di aggiornare $w$ si può alterare il comportamento dell'algoritmo, ad esempio:

-   se l'aggiornamento avviene dopo $l$ esempi, si ottiene il "\textcolor{blue}{Batch Algorithm}"
-   se l'aggiornamento viene fatto dopo ogni pattern si possono avere due differenti algoritmi:
    -   "\textcolor{green}{Mini-Batch algorithm}"
    -   "\textcolor{purple}{Stochastic algorithm}"


![LMS Algorithms](assets/lmsAlgo.png){width="33%"}


**Iperpiano**

Tipicamente i patterns sono rappresentati in una matrice quindi ci troviamo in uno spazio in più dimensioni e le formule dette prima valgono ancora ma con delle piccole modifiche per essere adattate al multidimensionamento:

$$W^{T}X = w_{0} + w_{1}x_{1} + w_{2}x_{2} + w_{3}x_{3} + ...$$

| Pattern | input$_{1}$ | input$_{2}$ | input$_{i}$ | input$_{n}$ |
|---------|-------------|-------------|-------------|-------------|
| $p_{1}$ | $x_{1,1}$   | $x_{1,2}$   | $x_{1,i}$   | $x_{1,n}$   |
| ...     | ...         | ...         | ...         | ...         |
| $p_{m}$ | $x_{m,1}$   | $x_{m,2}$   | $x_{m,i}$   | $x_{m,n}$   |

Table: esempio di matrice

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Linear Model in più dimensioni}}]
$$h(x_{p}) = X_{p}^{T}W = \sum_{i=0}^{n}x_{p,i}w_{i}$$
\end{tcolorbox}

dove $x_{p,i}$ indica l'input $i$-esimo del pattern $p$

**Problema:**\newline
_DATO_ un set di $l$ esempi di training _TROVARE_ il vettore dei pesi $w$ che minimizza la _Loss_ aspettata sui dati di training

$$E(w) = \sum_{p=1}^{l}(y_{p}-X_{p}^{T}w)^{2} = ||y - Xw||^{2}$$

$$\Delta w_{i} = -\frac{\partial E(w)}{\partial w_{i}} = -2\sum_{p=1}^{l}(y_{p}-h_{w}(x_{p}))x_{p,i} = -2\sum_{p=1}^{l}(y_{p}-X_{p}^{T}W))x_{p,i} = 0$$

\begin{algorithm}[H]
\DontPrintSemicolon
\SetKwFunction{Search}{Search}
\SetKwInOut{Input}{Input}
\SetAlgoNoLine
\SetAlgoNoEnd
\Input{VettorePesi, $\eta$}\tcp{vettore piccolo, $0<\eta < 1$ fissata}
\BlankLine
\While{!(converge) OR !($E_{w}$ è sufficiente piccola)}{

    calcola $\Delta_{w} = -\nabla(E_{w})$

    $w_{new}=w+\eta \Delta_{w}$
}

\caption{Gradient Descent}
\end{algorithm}

\newpage
![Learning Curves](assets/lmsCurves.png){width="33%"}


##### Linear Basis Expansion

La linearità del modello si riferisce ai coefficienti della regressione ($w$), questo permette di adattare il modello lineare anche a relazioni non lineari tra *input* e *output* mantenendo l'apprendimento attraverso il *Least Square* ma usando un polinomio del tipo

$$h_{w}(x) = w_{0} + w_{1}x + w_{2}x^{2} + ... + w_{M}x^{M} = \sum_{j=0}^{M}w_{j}x^{j}$$

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Linear Basis Expansion}}]
\emph{Il modello che si basa su dati non lineari si chiama Linear Basis Expansion e viene descritto dalla formula:}
$$h_{w}(x) = \sum_{k=0}^{K}w_{k}\phi_{k}(x)$$
\end{tcolorbox}

dove $\phi_{k}$ è una funzione $\phi_{k}: \mathbb{R}^{n} \rightarrow \mathbb{R}$ che trasforma le variabili $x$ per essere utlizzate in un vettore di input. \newline
Ad esempio $\phi(x)$ può rappresentare $x$

-   polinomiale: $\phi(x)=x_{j}^{2}$
-   trasformazione non lineare di singoli punti: $\phi(x) = log(x_{j})$
-   trasformazione non lineare di molti punti: $\phi(x) = ||x||$
-   ...

Quindi il modello è lineare su tutti i parametri compreso $\phi(x)$ (ma non in $x$), permettendoci di usare gli algoritmi del modello lineare come già visti.

Il vantaggio di questo modello è che è più espressivo, quindi può modellare relazioni più complesse. Questo comporta però che è necessario un metodo per controllare la complessità del modello viste le numerose funzioni che ora sono disponibili.

Infatti un modello troppo semplice porta ad un underfitting non adattandosi bene ai dati. Mentre un modello troppo complesso porta ad overfitting generando un modello sensibile alle perturbazioni.\newline
Motivo per il quale bisogna cercare un trade-off tra le due situazioni, andando a basarci sul _Rasoio di Occam_ preferendo, quindi, le ipotesi più semplici.

> La complessità non è il costo computazionale ma una misura della flessibilità del modello di adattarsi ai dati

##### Ridge Regression (Tikhonov Regularization)

Tramite la *ridge regression* è possibile aggiungere vincoli alla somma del valore di $|w_{j}|$ favorendo modelli sparsi con ad esempio meno termini (aventi peso $w_{j} = 0$). Questo causa una minore possibilità di andare in overfitting in quanto si ha, appunto, una minore varianza e quindi un modello smoothed.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Errore Ridge Regression}}]
$$Loss(h_{w}) = \sum_{p=1}^{l}(y_{p} - h_{w}(x_{p}))^{2} + \lambda ||w||^{2}$$
\end{tcolorbox}

dove $\lambda$ è la costante di regolarizzazione e $||w||^{2} = \sum_{i}w_{i}^{2}$ (norma 2 al quadrato).\newline
Andando a minimizzare l'errore in realtà si cerca di mantenere i $w_{i}$ bassi, in quanto la $Loss$ deve riuscire ad abbassare entrambi i termini.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Modello Ridge Regression}}]
$$w_{new}  = w + \eta \Delta w - 2\lambda w$$
\end{tcolorbox}

La *ridge regression* è applicabile in modo generale in quanto è possibile controllare la complessità del modello agendo solamente su $\lambda$ senza conoscere il grando del polinomio o la complessità del modello.

Il concetto chiave di questo modello è un corretto bilanciamento tra il primo e il secondo termine dell'equazione in quanto: 
- se si usa un valore di $\lambda$ troppo piccolo si va in contro ad un overfitting perché il termine degli errori sui dati diventa il "conducente" della complessità, 
- in caso contrario, se si usa un valore di $\lambda$ troppo grande si va in contro ad un modello in underfitting perché il "conducente" della complessità diventa il "regolatore" della complessità.

![bassa complessita: underfitting](assets/lbeUnder.png){width="33%"}

![complessità non eccessiva: fitting accettabile](assets/lbeFit.png){width="33%"}

![alta complessità: overfitting](assets/lbeUnder.png){width="33%"}

---

#### Classificazione

Il modello lineare usato nella regressione può essere adottato anche per la classificazione andando a creare un piano separatore dei dati. Tale zona di separazione viene detta _**decision boundary**_ ed è della forma:

$$w^{T}x = w_{0} + w_{1}x_{1}+w_{2}x_{2}+...+w_{n}x_{n} = 0$$

e $h(x) = sign(w^{T}x + w_{0})$ che sarà del tipo $h(x) = \left\{\begin{array}{ll} 1 & w^{T}x + w_{0} \ge 0 \\ 0(-1) & otherwise \end{array}\right.$

quindi in generale l'ipotesi di classificazione sarà definita dall'equazione (Linear Threshold Unit):

$$h(x_{p}) = sign(x_{p}^{T}w) = sign(\sum_{i=0}^{n}x_{p,i}w_{i})$$

La cosa da notare è che $w_{0}$ in questo caso ha il compito di Bias, il che significa che è il valore soglia. Diffatti dire $h(x) = w^{T}x + w_{0} \ge 0$ è equivalente a $h(x) = w^{T}x \ge -w_{0}$

Il problema dell'apprendimento per i classificatori lineari è:

**DATO** un set di $l$ esempi di training **TROVARE** $w$ per minimizzare la somma residua dei quadrati

e la Loss diventa (non si usa $h(x)$ come nella regressione perché nella cassificazione $h(x)$ è il segno e non il valore, il segno non è differenziabile)

$$E(w) = \sum_{p=1}^{l}(y_{p}-x_{p}^{T}w)^2 = ||y-Xw||^2$$

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Iperpiano Separatore}}]
\emph{Insieme di punti in cui il modello è nullo.}
\end{tcolorbox}

Il modello, quindi, viene usato per classificare i dati applicando la funzione _threshold_ $:= sign(wx)$. L'errore generato, invece, può essere computato come errore di classificazione o di numero di pattern mal classificati:

$$Loss(h(x_{p}), d_{p}) = \left\{\begin{array}{ll} 0 & h(x_{p})=d_{p} \\ 1 & otherwise \end{array}\right.$$

dove $meanError = \frac{1}{l \cdot \sum_{i=1}^{l}{L(h(x_{i}), d_{i})}}$ e il $numericError =\sum_{i=1}^{l}{L(h(x_{i}), d_{i})}$.\newline
Da questi valori è possibile estrarre l'accuratezza data dalla media dei classificati correttamente, e cioè: $acc = \frac{l-numericError}{l}$

---

### Support Vector Machine

La _Support Vector Machine (SVM)_  è un classificatore lineare derivato dalla _teoria di apprendimento statistico_ sviluppato da Vapnik. La _SVM_ dopo anni di sviluppo teorico diventa molto usata (famosa) quando prendendo immagini in input, restituiva i valori di riconoscimento della scrittura a mano paragonabili alle reti neurali (di quell'epoca). Attualmente la _SVM_ è largamente usata in tutte i campi di applicazioni del _supervised learning_.

Normalmente la _SVM_ viene discussa al termine di un corso approfondito di machine learning in quando è necessario avere un quadro molto dettagliato dell'argomento.\newline
In ogni caso è bene discuterne anche superficialmente in quando la maggioranza dei software (di ML) ne fanno uso.

Essendo così complicato l'argomento, l'obiettivo di questa introduzione è quello di 

1. avere una conoscienza sul controllo della complessità del modello attraverso un approccio di ottimizzazione
   - minimizzazione del _rischio strutturale_ dovuto al Maximum Margin Classifier 
2. usare in modo efficiente il _linear basis expansion_ attraverso il kernel
   - approccio flessibile per apprendimento supervisionato _non_ lineare
3. capire come evitare le tipiche mal interpretazioni durante l'uso della _SVM_

**[NB: Evitare di pensare a _SVM_ al di fuori delle esigenze di validazione]**

#### Maximum Margin Classifier

Prendendo un problema di classificazione binaria e supponendo di applicare una separazione lineare senza dati rumorosi, andremmo ad usare quello che viene detto **hard margin**.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Margin}}]
\emph{Il margine è il doppio della distanza tra il piano separatore (iperpiano) e i punti dei dati più vicini ad esso.}
\end{tcolorbox}

![Visione Geometrica di Margine](assets/marginDef.png){width="33%"}

Il problema è che di margini possono essercene molti e non tutti risolvono il problema nella medesima maniera, in più variando il piano varia anche il margine.

![Margini diversi](assets/margins.png){width="33%"}

Possiamo, quindi, intuitivamente definire il margine massimo come:
\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Hard Margin}}]
\emph{Il margine massimo è la distanza massima (safe-zone) tra i punti dei dati e il piano separatore}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Vettore di Supporto}}]
\emph{Si definisce vettore di supporto il vettore $x_{p}$ tale che} 
$$|w^{T}x_{p} + b | =1$$
\emph{con $b=w_{0}$}
\end{tcolorbox}

Cioè, i vettori per i quali l'iperpiano vale $1$ per i dati positivi e $-1$ per i dati negativi

Prendendo come esempio di supporto il problema di apprendimento per un modello lineare per classificazione binaria con funzione $h:\mathbb{R}^n \rightarrow \{-1,1\}$, $h$ sarà del tipo $h(x) = sign(wx+b)$. Possiamo definire il problema di training:

**Problema:**
_TROVARE_ $(w,b)$ tali che tutti i punti sono classificati correttamente e il margine è massimizzato\newline
ciò è verificato se e solo se $(w^{T}x_{p} + b)y_{p} \ge 1 \; \forall p$\newline

Formalmente:

$$Margine = \frac{2}{|w|}$$

quindi il margine è massimo $\iff$ $|w|$ è minimo $\iff$ $\frac{|w|^{2}}{2}$ è minimo

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{VC dimension}}]
\emph{La VC dimension del SVM è inverso al margine, quindi, diminuisce con un margine elevato}
\end{tcolorbox}

ciò comporta un controllo sulla complessità del modello da parte del margine.\newline
Da ciò si evince che l'iperpiano ottimale è quello che massimizza il margine (e quindi risolve il problema di training).

Dato il problema di training, descritto sopra, si può scrivere nella _**forma primale**_ come:

$$\text{Funzione obiettivo := } min(\frac{|w|^2}{2})$$
$$\text{Vincoli := } (w^{T}x_{p} +b)y_{p} \ge 1$$

Notare la diretta minimizzazione della complessità del modello nella funzione obiettivo con 0 errori nella soluzione nei vincoli.\newline
Nella _**forma duale**_:

$$\text{Funzione obiettivo := }  max(\sum_{i}\alpha_{i} - \frac{\sum_{i,j}\alpha_{i}\alpha_{j}y_{i}y_{j}x_{i}^{T}x_{j}}{2})$$
$$\text{Vincoli := } \alpha_{i} \ge 0 \land \sum_{i}\alpha_{i}y_{i} = 0$$
con $\alpha$, i moltiplicatori di Lagrange.

Dalla forma duale possiamo ricavarci $(w,b)$ calcolando:
$w = \sum_{p}\alpha_{p}y_{p}x_{p}$ e $b = y_{k} -w^{T}x_{k}$ con $\forall p\in [1,l]$ e $\forall \alpha_{k} > 0$

$$h(x) = sign(w^{T}x +b) = sign(\sum_{p=1}^{l}\alpha_{p}y_{p}x_{p}^{T}x+b) = sign(\sum_{p\in SV}\alpha_{p}y_{p}x_{p}^{T}x+b)$$

con $\alpha_{p} \not= 0 \implies x_{p}$ è un Support Vector. Ciò comporta che la soluzione è spesso sparsa e formulata solo in termini dei Support Vectors e l'iperpiano dipende solo da questi vettori.\newline
L'ultima forma di equazione è la forma più utile in quanto non è necessario calcolare esplicitamente $(w,b)$.

#### Soft Margin

L'_hard margin_ su tutti i punti potrebbe essere troppo restrittivo, quindi è possibile allentare un po' questo vincolo permettendo qualche errore sulla tolleranza dei dati e fornire un margine più largo. Per poter adottare questa soluzione è necessario introdurre le _**slack-variables**_ $\xi$, che saranno non nulle dove consentito l'errore.

Questo tipo di variabile vanno ad aggiungersi alle soluzioni del problema di training, diffatti il problema in vista primale diventa:\newline
$$\text{Funzione obiettivo := } min(\frac{|w|^2}{2}+C\sum_{p}{\xi_{p}})$$
$$\text{Vincoli := } (w^{T}x_{p} +b)y_{p} \ge 1-\xi_{p} \land \xi_{p} \ge 0$$

Più il valore di $C$ (che è un iperparametro) è basso più errori sul training set son permessi però c'è una grossa possibilità di finire in underfitting. Al contrario se il valore di $C$ è alto si può arrivare a non permettere errori sul training set e quindi arrivare all'overfitting. Questo però comporta che si perde l'automatica approssimazione del _Structural Risk Minimization_ dell'hard margin.

(Per passare dal _soft-margin_ all'_hard-margin_ **non** si deve porre $C=0$ ma dare a $C$ un valore molto alto, questo dovuto al fatto che il nel _soft-margin_ sono presenti le _slack-variables_)

#### Kernel

Per problemi non lineari non è possibile utilizzare gli strumenti visti fino ad ora, in quanto classificano modelli lineari.\newline
Per poter trattare casi non lineari si utilizza, in modo efficiente, la _linear basis expansion_ attraverso il _**kernel**_, ottenendo così un ulterirore approccio flessibile per il modello di _supervised learning non lineare_.

Per poter separare i dati in modo lineare anche se tali dati nello spazio di input non li sono, si mappano in uno spazio in alte dimensioni in modo che l'espansione li renda linearmente separabili, ad esempio con un'applicazione del tipo:
$\Phi: \mathbb{R}^2 \rightarrow \mathbb{R}^3$. Tale spazio in alte dimensioni si chiama _**Feature Space**_.

Assumendo di usare la _Linear Basis Expansions_, per poter essere usata in questo modello, si va a modificare l'uso del vettore di input $x$ con $\Phi(x)$. Tale processo può portare però a grossi problemi come ad esempio la difficoltà/infattibilità computazionale e overfitting se non si controlla la dimensione dello spazio e la complessità del classificatore.
\newline
Con questa modifica quindi la _LBE_ diventa:

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{LBE non lineare}}]
$$h_{w}(x) = sign(\sum_{k}w_{k}\Phi_{k}(x))$$
\end{tcolorbox}

Come già visto, nella _SVM_, non è necessario calcolare $w$, in più, i dati sono rappresentati come prodotto scalare tra coppie di punti. Quindi:

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{LBE non lineare}}]
$$h(x) = sign(\sum_{p\in SV}\alpha_{p}y_{p}\Phi^{T}(x_{p})\Phi(x))$$
\end{tcolorbox}

Ma qui ci viene in aiuto la funzione _**Kernel**_ che ci permette di non calcolare le varie $\Phi()$, andando a costruiore la funzione in questo modo:

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Kernel}}]
$$h(x) = sign(\sum_{p\in SV}\alpha_{p}y_{p}K(x_{p}x))$$
\end{tcolorbox}

permettendoci comunque di gestire il _feature space_ ma attraverso la funzione $K()$

![Espansione](assets/kernelsvm.jpg){width="33%"}

\begin{tcolorbox}[colback=white!5!white, colframe=black,title=Esempi di funzioni \emph{Kernel}]
    \begin{itemize}
        \item \textbf{\textit{Lineare}}: $K(x_{i},x_{j}) = x_{i}^{T}x_{j} \implies x \rightarrow \Phi(x)$ con $\Phi(x)=x$ 
        \item \textbf{\textit{Polinomiale}}: $K(x_{i},x_{j}) = (1+x_{i}^{T}x_{j})^{k} \implies x \rightarrow \Phi(x)$ con $\Phi(x)$ polinomia in $k$
        \item \textbf{\textit{Radial-Basis Function}}: $K(x_{i},x_{j}) = e^{-\frac{||x_{i}-x_{j}||}{2\sigma^{2}}} \implies x \rightarrow \Phi(x)$ con $\Phi(x)$ dimensionalmente infinita
    \end{itemize}
\end{tcolorbox}

Combinando l'uso efficiente della _linear basis expansions_ tramite il _kernel_ con un _max margin_, aiuta ad avere modelli flessibili ma controllandone la complessità. 

#### Uso

Uno tipo di errore pratico è quello di non considerare la _SVM_ in grado di andare in overfitting. L'overfitting si può verificare molto facilmente se si porge poca attenzione alla scelta degli iperparametri $C$, Kernel, ... .

---

### K-Nearest Neighbors

Dati i dati di input $x$, salva i dati di training nella codifica $<x_{p},y_{p}> \; \forall p \in [1,l]$ ^[NOTA: la fase di training per _K-NN_ consiste solo nel salvataggio dei dati di input] e trova i k-esimi vicini più vicini, utilizando ad esempio la distanza euclidea (norma 2) per _1-NN_. Con questo tipo di classificazione non otteniamo una classificazione lineare ma una abbastanza **irregolare**.

Lavorando invece su _k_ vicini, il modo più naturale è quello di guardare tutti i vicini e poi fare la media di questi 

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Distanza Media dei vicini}}]
$$avg_{k} = \frac{1}{k}\sum_{x_{i} \in N_{k}(x)}y_{i}$$
\end{tcolorbox}

con $N_{k}(x)$ l'insieme dei vicini di $x$ che contiene esattamente $k$ vicini. 

La regola di classificazione è quindi la maggioranza tra i membri di $N_{k}(x)$, formalmente

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{K-NN}}]
$$h(x) = \left\{\begin{array}{ll} 1 & avg_{k}(x) > 0.5 \\ 0(-1) & otherwise \end{array}\right.$$
\end{tcolorbox}

Anche qui è necessaria una scelta oculata per il numero di vicini da guardare, cioè sul valore di $K$,  in quanto ne comporta l'under o l'over fitting. Infatti, più alto sarà il valore di $K$ (più vicini) più si tende ad un _underfitting_, mentre più basso sarà il valore di $K$ (meno vicini) più si tende all'_overfitting_

In ogni caso questo metodo è molto costoso a livello computazionale, in quanto, per ogni nuovo input si calcola la distanza dal target e da _**tutti**_ i vettori salvati fino a quel momento, e a livello di spazio in quanto è necessario mantenere tutti i dati.

Le particolarità/criticità di questo approccio sono che:

- non fa ipotesi globali, di conseguenza non ha un modello per cui fare il fitting, infatti si salvano solo i dati
- il punto critico è la misura della distanza, una volta fatta quella l'algoritmo è terminato
- non eseguendo training, il costo computazionale è spostato totalmente nella fase di predizione
- quando si hanno molte variabili in input, _K-NN_ può fallire spesso per la _"maledizione della dimensionalità" (**Curse of Dimension**)_ in quanto la quantità di dati necessaria a supportare il risultato cresce _esponenzialmente_ con la dimensionalità
- se il target dipende _solo_ da alcune features e non da tutte, allora si può fare il retrieval di un pattern "simile", dove la somiglianza è dominanta dal grande numero di features irrilevanti, per questo motivo questo problema viene detto _"maledizione del rumoroso" (**Curse of Noisy**)_

---

---

## Unsupervised Learning

Utilizzando un approccio di _Unsupervised Learning_ non si ha un insegnante, quindi i dati nel training set sono senza valore di target: $<x>$.

Per dividerli si utilizza la loro natura, come ad esempio:

1.  clustering
2.  dimensionality reduction / visualization / preprocessing
3.  modelling the data density

L'utilità di questo modello di learning si può vedere nel _**data analysis**_ dove è necessario:

- esplorare i dati per:
    - scoprire la struttura dei dati
    - trovare un schema sconosciuto
- preprocessare i dati per altri approcci nel machine learning
- mantenere un basso costo in termini di spazio, in quanto non si utilizzerebbe il label di questi dati.


### Clustering

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Clustering}}]
\emph{Il Clastering è il partizionamento di dati in sottoinsiemi con proprietà simili. I pattern all'interno di un cluster valido hanno più similituini tra di loro di quanto ne avrebbero con un modello appartenente ad un altro cluster.}
\end{tcolorbox}

Ogni cluster ha un suo **centroide**, un prototipo.

Il goal nello spazio delle ipotesi per il clustering è quello di trovare l'ottimo partizionamento su una distribuzione sconosciuta nello spazio di input $X$.\newline
$H:x\rightarrow c(x)$ dove H è un insieme di vettori quantizzatori da uno spazio continuo $x$ ad uno spazio discreto $c(x)$ che rappresenta il cluster. Un esempio è la funzione $Loss$ _Squared Error Distortion_:

$$L(h(x_{p})) = ||x_{p} - c(x_{p})||^{2}$$

![Clusters e Centroidi](assets/clusters.png){width="33%"}


#### K-means

_K-means_ è il più semplice da implementare e il più comune algorimo usato che adotta il criterio _squared error_ ed è anche generalmente efficiente.

\begin{algorithm}[H]
\DontPrintSemicolon
\SetAlgoNoLine
\SetAlgoNoEnd
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\Input{D, k}\tcp{D = data points, k = $\#$ clusters}
\Output{Data points with cluster membership}
\BlankLine
\Repeat{a convergence criterion is not met}{

1. Assign each pattern to the closest cluster center (\emph{winner})

2. Recompute the cluster center using current cluster membership

}\tcp{(no reassignment of patterns to new cluster centers or\newline minimal decrease in squared error)}

\caption{K-means Batch Algorithm}
\end{algorithm} 

In altre parole l'algoritmo esegue i seguenti passi:\newline
Dati i centri dei cluster $c_{1}, ..., c_{k}$

(1.) per ogni winner calcola la distanza minima pari a: $||x - C_{i}||^{2} = \sum_{j}^{n}{(x_{j}-c_{ij})}^{2}$

(2.) per ogni cluster $i$ la nuovo "mean" (centroide) è: $c_{i} = \frac{1}{|cluster_{i}|}\sum_{j:x_{j}\in cluster_{i}}x_{j}$


![Suddivisione dei dati prima dell'esecuzione](assets/datakmeans.png){width="33%"}

![Suddivisione dei dati dopo l'esecuzione](assets/kmeans.png){width="33%"}

Le particolarità/criticità di questo algoritmo sono che:

- per trovare il valore di $k$ più adatto è necessario eseguire più volte l'algoritmo (_"trial-and-error"_)
- i minimi locali della $Loss: \; L(x)$ rendono il metodo dipendente da come viene inizializzato (_"trial-and-error"_ anche in questo caso)
- funziona nel migliore dei modi quando lavora si cluster compatti e ipersferici
- non consente di proiettare i dati su uno spazio di dimensione minore

---

### Preprocessing

- _**Dimensionality reduction:**_ dati $n$ punti di input si va a ridurne il numero in $n'$ \newline $<x_{1}, x_{2}, ..., x_{n}> \rightarrow <x_{1}', x_{2}', ..., x_{n'}'>$ con $n' < n$.

![PCA](assets/pca.png){width="33%"}

- _**Feature Selection:**_ si sceglie un sottoinsieme di tutte le features e automaticamente si sceglie quella corretta in base della sua rindondanza o rilevanza nel task. Per questo è necessario conoscere il dominio. Tale problema è difficile quando un problema di learning ma è utile per ridurre il rumore o come risultato stesso.

- _**Outlier Detection:**_ si trovano i valori di dati inusuali che non sono consistenti con la maggioranza delle osservazioni


### Altri Learining

#### Reinforcement Learning

Questo tipo di learning è utilizzato su sistemi autonomi come ad esempio i robot. L'algoritmo apprende una politica su come agire data un'osservazione sul suo mondo. Ogni azione fatta produce un effetto sull'ambiente il quale restrituisce un feedback. Tale feedback è il "sensore" che guida l'algoritmo nell'apprendere. Al posto di avere un supervisore, ad ogni passo c'è un'informazione sulle vincite o perdite per lo stato finale. La politica delle azioni è la necessità di massimizzare le ricompense ricevute. Sarà il meccanismo di apprendimento che deciderà quali azioni sono state maggiormente utili per la vittoria o per la perdita.

![Reinforcement Learning](assets/relearning.png){width="33%"}


#### Altri

- _**Semi-Supervised Learning:**_ combina esempi etichettati ed esempi non etichettati (quest'ultimi in numero maggiore) per generare una funzione o classificatore appropriato
- _**Learn to Rank:**: lista di input in ingresso e stessa lista di input in uscita ma ordinata (search engines)
- _**Online (continuos) Learning:**_ nuovi esempi vengono appresi durante il ciclo di vita
- _**Structured Domain Learning and Relational Learnin:**_ il dominio di input e/o output può essere strutturato sotto forma di sequenza o con strutture più complesse come alberi, grafi.

##### Neural Networks

Le reti neurali sono un paradigma adottabile sia per il supervised che per il unsupervised learning.\newline
L'idea di base è molto simile al _Linear Threshold Unit_, infatti, _LTU_ è l'unità base delle _Neural Network_ detta _**perceptron**_. Una rete neurale è una rete di queste unità _non lineari_ con capacità di approssimazione universale in grado di comporre un approccio molto flessibile per qualunque scopo del _ML_. Nel dettaglio ad esempio si può pensare ad usare un gradiente discendete per l'apprendimento e una propagazione all'indeitro dell'errore dai layer di output ai layer di input.

Le reti neurali nascono come paradigma computazionale negli anni 40 del $XX$ secolo su ispirazione delle reti neurali biologiche. Ora sono diventate un modello computazionale molto potente per funzioni di approssimazione con capacità predittive, supportatate da una rigorosa base teorica.

![Single-Layer Neural Network](assets/singleNN.png){width="33%"}

![Multi-Layer Neural Network](assets/multiNN.png){width="33%"}

I livelli interni detti _hidden layers_ con unità non lineari forniscono alle reti neurali capacità di astrarre dall'apprendimento nuove rappresentazioni dei dati e tali rappresentazioni semplificano la classificazione del task all'ultimo layer. In più danno la _adaptive basis expansion_ non lineare in $w$, dove le $\Phi$ vengono apprese, di conseguenza $\Phi(x,w)$ dipenderà da $x$ e da $w$. Inoltre, rendono la rappresentazione distribuita e trattare le somiglianze può essere più facile che trattare vettori a valori reali.

###### Deep Learning

Il deep learning è un esempio di una struttra di reti neurali a multi-livelli a diverse profondità. Tale struttura fornisce una struttura a livelli gerarchici, dal più basso al più alto, con diversi livelli di astrazione e quindi diverse rappresentazioni delle features. Valido, ovviamente, sia per il supervised che per il unsupervised learning.\newline
Ad esempio, un immagine può essere rappresentata come un vettore di valori di intensità dei pixel o come un insieme di angoli o con regioni di forme particolari o ... . Tali rappresentazioni possono essere differenti in ogni livello delle reti neurali. \newline
In ogni caso, la peformance migliore c'è quando l'informazione in ingresso ha una qualche forma di struttura e le caratteristiche astratte possono essere riusate ai livelli più alti, ad esempio combinandole per generalizzare su casi mai visti. \newline
(Esempio: foto di donna senza occhiali + foto di uomo con occhiali da sole = foto di donna con occhiali da vista)

---

---

## Generalizzazione

Learning significa cercare una buona funzione, rispetto alla generalizzazione dell'errore, nello spazio delle funzioni da dati non conosciuti. Tale funzione misura la precisione del modello  nel predire accuratamente su nuovi campioni di dati, quindi più basso sarà l'errore più alta sarà la precisione e viceversa. Da ciò si evince che la capacità di generalizzazione di una funzione è il punto chiave del machine learning.

Per fare questo c'è la necessità di dividere le fasi di sviluppo in:

- _learning_: creare il modello da dati conosciuti
- _predective_: applicare nuovi esempi e valutare l'ipotesi predittiva

Si misura la performance nel machine learing in base all'accuratezza della predizione.\newline
Per la **classificazione** tale misura si riferisce al _Mean Square Error_, per la _Loss_, e al _Mean Error Rate_, per il risultato. Mentre per la **regeressione** si fa riferimento al _Mean Square Error_, _Mean Absolute Error_. In ogni caso un **alto error** significa una **bassa accuratezza** in qualunque fase di sviluppo ci si trovi.

### Validazione

Per la validazione del modello ci sono, anche qui, due fasi ben distinte

1. _**Model Selection**_: in questa fase si stima la performance (capacità di generalizzazione) di differenti modelli di learning per scegliere il migliore.
	- Significa cercare il miglior iperparametro (ordine del polinomio, $\lambda$ della ridge regressio, ...)
2. _**Model Assessment**_: scelto il modello finale, stimare l'errore sulla previsione.
	- Necessario usare un nuovo set di dati (test set)

![Divisione Data Set](assets/DATASET.png){width="33%"}

Il set di training e di validation sono dati che devono essere utilizzati **solo** durante la fase di _model selection_ e questi due insiemi possono anche "comunicare" tra loro.\newline
Invece è **molto** importante che l'insieme di _test_ venga usato **solamente** dopo essere sicuri di avvere il modello corretto e **solamente** durante la fase di _model assessment_.\newline
Altra cosa molto importante è che, una volta entrati nella fase di _model assessment_, non si può retrocedere nelle fasi precedenti.

Tutte queste precauzioni sono necessarie per essere certi di avere un modello corretto e realmente funzionante.

![Dataset Usage](assets/datasetUse.png){width="33%"}


\begin{algorithm}[H]
\DontPrintSemicolon
\SetAlgoNoLine
\SetAlgoNoEnd
\SetKwFunction{Search}{Search}
\SetKwFunction{Best}{Best}
\SetKwFunction{Select}{Select}
\SetKwFunction{Eval}{Eval}
\SetKwInOut{Input}{Input}
\Input{TR, VL, TS}\tcp{TR = Training Set, VL = Validation Set, TS = Test Set}
\BlankLine
\Search{\Best{$h_{w,\lambda}$}}\emph{ changing the model hyperparameter}\;
\ForEach{different value of $\lambda$}{$Search({Best({h_{w,\lambda}})}$\emph{ that minimize error/empirical loss finding the best $w$ parameters) }\tcp{Best = minimum error on TR}}
\Select{\Best{$h_{w,\lambda}$}}\tcp{Best = minimum error on the VL}

\Eval{final $h_{w,\lambda}$}\emph{ on TS}
\caption{Global Meta Algorithm}
\end{algorithm} 

La ricerca del miglior iperparametro può essere un ciclo _for_ su una griglia di valori candidati. Per ogni modello, $h_{w,\lambda}$ è "addestrato" a calcolare i risultati sul *VL* e poi prendere quello con errore minimo.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Iperparametro}}]
\emph{Il parametro che non viene appreso direttamente e che non viene modificato dal learning si chiama \textbf{iperparametro}}
\end{tcolorbox}

_**Esempio Negativo di utilizzo:**_\newline
Dati 20/30 esempi, 1000 variabili in input, il target random su 0/1, si sceglie un modello con una sola variabile che indovina (per puro caso) al 99% sul data set _e poi su un qualsiasi split successivo (training, validation, test)_. 99% **NON** è una buona stima, il valore corretto della stima è 50% in quanto:

- l'errore stimato su training/validation per la model selection non è utile per la stima dell'errore
- usare tutto il data set per model selection non è corretto per la stima
    - era necessario esegurie la separazione prima di fare la model selection

Solo se si usasse un test set esterno si otterebbe la stima corretta.


#### K-fold Cross Validation

1. divide **l'intero** data set $D$ in $k$ sottoinsiemi mutualmente esclusivi
2. allena l'algoritmo di apprendimento su $D \setminus D_{i}$ insiemi e lo testa sull'insieme $D_{i}$
3. riassume la media di tutti i $D_{i}$ risultati (_diagonalizza_)

Questo algoritmo può essere usato sia per la validazione che per il testing. La proprietà principale del _K-fold Cross Validation_ è che quando ha finito ha usato l'intero data set per tutti i compiti: training, validation, test

I problemi di questa tecnica sono però che:

- potremmo essere incerti su quanti divisioni fare sul data set
- potrebbe essere computazionalmente molto costo
- si può usare la stessa tecnica su ogni fold sviluppato o innestarlo ad altre tecniche complicando parecchio il procedimento

> _Alto "Error Rate" significa modello semplice e quindi possibile underfitting. Al contrario, basso "Error Rate" significa modello complesso e quindi possibile overfitting._

![Error VS Model](assets/erVSmd.png){width="33%"}

La relazione tra error rate e model complexity può essere attenuata non solo da un loro compromesso ma anche dal numero di dati in input, infatti, un elevato numero di dati in input aiuta il modello a sistemare la curva vicino al target.

La capacità di generalizzazione di un modello, misurata come errore di test, rispetto all'errore di training e nelle zone di over/under-fitting, dipende dalla complessità del modello e dal numero di dati.

 | 
- | - 
![Overfittig](assets/lbeOver.png){width="33%"} | ![Better Fitting](assets/overBUTok.png){width="33%"}

---

### Statistical Learning Theory (SLT)

**Problema:**
_DATI_ il valore dell'esempio teacher $d$, la distribuizione di probabilità e una funzione $Loss = (d-h(x))^{2}$, _CERCA_ $h$ in $H$ tale che minimizzi $R = \int L(d,h(x))dP(X,d)$ su un insieme di dati finito $TR=(x_{p},d_{p})$.

Per trovare $h$ si minimizza quindi il _rischio empirico_, trovando il miglior valor per i parametri liberi del modello.

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{Rischio empirico}}]
$$R_{emp} = \frac{1}{l}\sum_{p=1}^{l}(d_{p}-h(x_{p}))^{2}$$
\end{tcolorbox}

#### Empirical Risk Minimization: Vapnik-Chervonenkis dimension and SLT

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{VC dimension}}]
\emph{La VC dimension (VC-dim) è la misura della complessità di $H$, cioè la capacità di flessibilità nell'adattarsi ai dati}
\end{tcolorbox}

\begin{tcolorbox}[coltitle=black!70!white, colback=yellow!10!white, colframe=orange!70!yellow!55!white,title=Definizione \textbf{\emph{VC bounds}}]
\emph{Il VC bounds, con probabilità $1-\delta$, ritiene che rischio garantito $R$ è}
$$R \le R_{emp} + \epsilon (\frac{1}{l}, VC-dim, \frac{1}{\delta})$$
\end{tcolorbox}

con $\epsilon =$ _VC confidence_ è una funzione proporzionale a VC-dim e inversamente a $l$ e $\delta$.

Sappiamo che $R_{emp}$ decresce con modelli complessi, cioè con un'alta VC-dim.\newline
$\delta$ è la "confidenza", cioè regola la propabilità che il vincolo vali.

Quindi, più $l$ sarà alto (quindi più dati ci saranno) più la _VC confidence_ sarà bassa e quindi il limite sarà più vicino a $R$.

Un modello troppo semplice, quindi _VC-dim_ bassa, può non essere sufficiente a causa dell'elevato $R_{emp}$ causando _undefitting_.

Più elevata è la _VC-dim_ (con $l$ fissato), più $R_{emp}$ sarà basso, ma _VC confidence_, e quindi $R$, potrebbero alzarsi causando _overfitting_.

![Limite su R](assets/eVSvcd.png){width="33%"}

La _SLT_ permette un trattamento formale del problema della generalizzazione e del over/under-fitting fornendo limitazioni superiori al rischio $R$ di predizione su tutti i dati **indipendentemente** dal tipo di algoritmo di apprendimento e dai dettagli del modello.

Questo permette di asserire che il machine learning è ben fondato in quanto il rischio di learning può essere analiticamente limitato e si può trovare una buona approssimazione della $f$ target, a patto di avere un consistente numero di dati e una adeguata complessità del modello, misurabile con la VC-dim.

Questa teoria porta alla creazione di nuovi modelli (es. SVM) e fonda uno dei principi induttivi sul controllo della complessità.

#### Esempi di controllo della complessità

- _**Linear Model:**_
	- la complessità è legata al numero dei parametri liberi $w$
		- dimensionsione dell'input
		- grado del polinomio
- _**Decision Tree:**_
	- il controllo della complessità può dipendere dal numero di nodi
		- controllando quando stoppare la discesa
		- facendo pruning

---

## Applicazioni del _Machine Learning_

Il _Machine Learning_ va applicato per risolvere problemi reali da trattare però tradizionalmente o per problemi dove non sono sufficienti le conoscenze umane o in cui sono richiesti comportamenti personalizzati.

Esempi:

- riconoscimento dello ZIP (CAP)
- OCR: riconoscimento della scrittura a mano libera
- AlphaGO: gioco 
	- _Value Network_ per valutare le posizioni e politica di mosse.
	- deep neural network in costante allenamento
- veicoli guida autonomamente
- pattern recognition
- data mining
- knowledge discovery
- computational intelligence
- deep learning

---

---
 
\newpage
