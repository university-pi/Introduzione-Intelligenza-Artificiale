\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{\numberline {1}Risoluzione di Problemi come Ricerca}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Agenti}{3}{section.1.1}% 
\contentsline {subsection}{\numberline {1.1.1}Descrizione dell'agente e dell'ambiente}{3}{subsection.1.1.1}% 
\contentsline {subsubsection}{\numberline {1.1.1.1}Agente}{3}{subsubsection.1.1.1.1}% 
\contentsline {subsubsection}{\numberline {1.1.1.2}Ambiente}{4}{subsubsection.1.1.1.2}% 
\contentsline {section}{\numberline {1.2}Problem Solving}{4}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Modo di procedere}{4}{subsection.1.2.1}% 
\contentsline {subsubsection}{\numberline {1.2.1.1}Formulazione del problema}{4}{subsubsection.1.2.1.1}% 
\contentsline {subsection}{\numberline {1.2.2}Algoritmi di Ricerca}{4}{subsection.1.2.2}% 
\contentsline {subsubsection}{\numberline {1.2.2.1}Misura delle prestazioni}{5}{subsubsection.1.2.2.1}% 
\contentsline {subsubsection}{\numberline {1.2.2.2}Alberi di ricerca}{5}{subsubsection.1.2.2.2}% 
\contentsline {subsubsection}{\numberline {1.2.2.3}STRATEGIE NON INFORMATE}{5}{subsubsection.1.2.2.3}% 
\contentsline {subsubsection}{\numberline {1.2.2.4}STRATEGIE INFORMATE (Ricerca Euristica)}{6}{subsubsection.1.2.2.4}% 
\contentsline {subsubsection}{\numberline {1.2.2.5}RICERCHE LOCALI (strategie informate)}{7}{subsubsection.1.2.2.5}% 
\contentsline {paragraph}{\numberline {1.2.2.5.1}Spazi continui}{9}{paragraph.1.2.2.5.1}% 
\contentsline {section}{\numberline {1.3}Giochi con Avversario}{9}{section.1.3}% 
\contentsline {subsection}{\numberline {1.3.1}Algoritmi di risoluzione}{9}{subsection.1.3.1}% 
\contentsline {subsubsection}{\numberline {1.3.1.1}Formulazione del problema}{9}{subsubsection.1.3.1.1}% 
\contentsline {subsubsection}{\numberline {1.3.1.2}Algoritmi}{10}{subsubsection.1.3.1.2}% 
\contentsline {section}{\numberline {1.4}CSP: Constraint Satisfaction Problems}{11}{section.1.4}% 
\contentsline {subsection}{\numberline {1.4.1}Modo di procedere}{11}{subsection.1.4.1}% 
\contentsline {subsubsection}{\numberline {1.4.1.1}Formulazione del problema}{11}{subsubsection.1.4.1.1}% 
\contentsline {subsection}{\numberline {1.4.2}STRATEGIE DI RICERCA NON INFORMATE}{11}{subsection.1.4.2}% 
\contentsline {subsubsection}{\numberline {1.4.2.1}Codici}{12}{subsubsection.1.4.2.1}% 
\contentsline {subsection}{\numberline {1.4.3}STRATEGIE DI RICERCA INFORMATE}{12}{subsection.1.4.3}% 
\contentsline {chapter}{\numberline {2}Rappresentazione della Conoscenza e Ragionamento}{14}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Agenti Basati su Conoscenza}{14}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Formulazione della KB}{14}{subsection.2.1.1}% 
\contentsline {section}{\numberline {2.2}Calcolo Proposizionale}{14}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Model Checking}{15}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Algoritmi per la Soddisfacibilit\IeC {\`a}}{15}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Algoritmi locali per la Soddisfacibilit\IeC {\`a}}{16}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Inferenza come deduzione}{16}{subsection.2.2.4}% 
\contentsline {section}{\numberline {2.3}Logica del Primo Ordine}{16}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Regole di Inferenza}{16}{subsection.2.3.1}% 
\contentsline {subsubsection}{\numberline {2.3.1.1}Teorema di Herbrand}{17}{subsubsection.2.3.1.1}% 
\contentsline {subsection}{\numberline {2.3.2}Trasformazione a Clausole}{17}{subsection.2.3.2}% 
\contentsline {subsubsection}{\numberline {2.3.2.1}Unificazione}{18}{subsubsection.2.3.2.1}% 
\contentsline {subsubsection}{\numberline {2.3.2.2}Risoluzione per refutazione}{18}{subsubsection.2.3.2.2}% 
\contentsline {subsection}{\numberline {2.3.3}Strategie di risoluzione}{18}{subsection.2.3.3}% 
\contentsline {chapter}{\numberline {3}Introduzione al Machine Learning}{20}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Supervised Learning}{21}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Concept learning}{22}{subsection.3.1.1}% 
\contentsline {subsubsection}{\numberline {3.1.1.1}Regole congiuntive}{22}{subsubsection.3.1.1.1}% 
\contentsline {paragraph}{\numberline {3.1.1.1.1}FIND-S}{23}{paragraph.3.1.1.1.1}% 
\contentsline {paragraph}{\numberline {3.1.1.1.2}Version Space}{23}{paragraph.3.1.1.1.2}% 
\contentsline {paragraph}{\numberline {3.1.1.1.3}Candidate Elimination}{24}{paragraph.3.1.1.1.3}% 
\contentsline {paragraph}{\numberline {3.1.1.1.4}Decision Tree}{25}{paragraph.3.1.1.1.4}% 
\contentsline {paragraph}{\numberline {3.1.1.1.5}ID3}{25}{paragraph.3.1.1.1.5}% 
\contentsline {subparagraph}{\numberline {3.1.1.1.5.1}Entropia}{26}{subparagraph.3.1.1.1.5.1}% 
\contentsline {subparagraph}{\numberline {3.1.1.1.5.2}Gain}{26}{subparagraph.3.1.1.1.5.2}% 
\contentsline {subparagraph}{\numberline {3.1.1.1.5.3}Gain Ratio}{27}{subparagraph.3.1.1.1.5.3}% 
\contentsline {paragraph}{\numberline {3.1.1.1.6}Problemi e Soluzioni}{28}{paragraph.3.1.1.1.6}% 
\contentsline {paragraph}{\numberline {3.1.1.1.7}Overfitting}{28}{paragraph.3.1.1.1.7}% 
\contentsline {paragraph}{\numberline {3.1.1.1.8}Stopping}{28}{paragraph.3.1.1.1.8}% 
\contentsline {paragraph}{\numberline {3.1.1.1.9}Pruning}{29}{paragraph.3.1.1.1.9}% 
\contentsline {subparagraph}{\numberline {3.1.1.1.9.1}Regole di Post-Pruning}{29}{subparagraph.3.1.1.1.9.1}% 
\contentsline {paragraph}{\numberline {3.1.1.1.10}Costo degli attributi}{29}{paragraph.3.1.1.1.10}% 
\contentsline {subsubsection}{\numberline {3.1.1.2}Bias}{29}{subsubsection.3.1.1.2}% 
\contentsline {paragraph}{\numberline {3.1.1.2.1}Bias Induttivo}{30}{paragraph.3.1.1.2.1}% 
\contentsline {subsection}{\numberline {3.1.2}Linear Model}{31}{subsection.3.1.2}% 
\contentsline {subsubsection}{\numberline {3.1.2.1}Regressione}{31}{subsubsection.3.1.2.1}% 
\contentsline {paragraph}{\numberline {3.1.2.1.1}Univariate Linear Regression}{31}{paragraph.3.1.2.1.1}% 
\contentsline {paragraph}{\numberline {3.1.2.1.2}Least Mean Square}{32}{paragraph.3.1.2.1.2}% 
\contentsline {paragraph}{\numberline {3.1.2.1.3}Local Search}{33}{paragraph.3.1.2.1.3}% 
\contentsline {paragraph}{\numberline {3.1.2.1.4}Linear Basis Expansion}{35}{paragraph.3.1.2.1.4}% 
\contentsline {paragraph}{\numberline {3.1.2.1.5}Ridge Regression (Tikhonov Regularization)}{35}{paragraph.3.1.2.1.5}% 
\contentsline {subsubsection}{\numberline {3.1.2.2}Classificazione}{37}{subsubsection.3.1.2.2}% 
\contentsline {subsection}{\numberline {3.1.3}Support Vector Machine}{38}{subsection.3.1.3}% 
\contentsline {subsubsection}{\numberline {3.1.3.1}Maximum Margin Classifier}{38}{subsubsection.3.1.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.3.2}Soft Margin}{40}{subsubsection.3.1.3.2}% 
\contentsline {subsubsection}{\numberline {3.1.3.3}Kernel}{41}{subsubsection.3.1.3.3}% 
\contentsline {subsubsection}{\numberline {3.1.3.4}Uso}{42}{subsubsection.3.1.3.4}% 
\contentsline {subsection}{\numberline {3.1.4}K-Nearest Neighbors}{42}{subsection.3.1.4}% 
\contentsline {section}{\numberline {3.2}Unsupervised Learning}{43}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Clustering}{43}{subsection.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.1.1}K-means}{44}{subsubsection.3.2.1.1}% 
\contentsline {subsection}{\numberline {3.2.2}Preprocessing}{45}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Altri Learining}{45}{subsection.3.2.3}% 
\contentsline {subsubsection}{\numberline {3.2.3.1}Reinforcement Learning}{45}{subsubsection.3.2.3.1}% 
\contentsline {subsubsection}{\numberline {3.2.3.2}Altri}{46}{subsubsection.3.2.3.2}% 
\contentsline {paragraph}{\numberline {3.2.3.2.1}Neural Networks}{46}{paragraph.3.2.3.2.1}% 
\contentsline {subparagraph}{\numberline {3.2.3.2.1.1}Deep Learning}{47}{subparagraph.3.2.3.2.1.1}% 
\contentsline {section}{\numberline {3.3}Generalizzazione}{47}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Validazione}{47}{subsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.1.1}K-fold Cross Validation}{49}{subsubsection.3.3.1.1}% 
\contentsline {subsection}{\numberline {3.3.2}Statistical Learning Theory (SLT)}{50}{subsection.3.3.2}% 
\contentsline {subsubsection}{\numberline {3.3.2.1}Empirical Risk Minimization: Vapnik-Chervonenkis dimension and SLT}{50}{subsubsection.3.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.3.2.2}Esempi di controllo della complessit\IeC {\`a}}{51}{subsubsection.3.3.2.2}% 
\contentsline {section}{\numberline {3.4}Applicazioni del \emph {Machine Learning}}{51}{section.3.4}% 
